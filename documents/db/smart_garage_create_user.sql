SELECT DEFAULT_COLLATION_NAME FROM 'information_schema'.'SCHEMATA' WHERE SCHEMA_NAME='smart_garage';
SHOW TABLE STATUS FROM smart_garage;
SHOW FUNCTION STATUS WHERE Db='smart_garage';
SHOW PROCEDURE STATUS WHERE Db='smart_garage';
SHOW TRIGGERS FROM smart_garage;
SELECT *, EVENT_SCHEMA AS Db, EVENT_NAME AS Name FROM information_schema.EVENTS WHERE EVENT_SCHEMA='smart_garage';

USE smart_garage;
FLUSH PRIVILEGES;
SHOW COLUMNS FROM mysql.user;
SELECT user, host, IF(LENGTH(password)>0, password, authentication_string) AS password FROM mysql.user;
CREATE USER 'smartGarageUser'@'localhost' IDENTIFIED BY 'smartGarageUser';
GRANT USAGE ON *.* TO 'smartGarageUser'@'localhost';
GRANT EXECUTE, SELECT, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES  ON `smart\_garage`.* TO 'smartGarageUser'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'smartGarageUser'@'localhost';
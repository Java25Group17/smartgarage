create or replace table currency
(
	id int auto_increment
		primary key,
	name varchar(20) not null,
	abbreviation varchar(3) not null,
	default_exchange_course double not null,
	constraint currency_abbreviation_uindex
		unique (abbreviation)
);

create or replace table currency_current_date
(
	id int auto_increment
		primary key,
	date date not null
);

create or replace table engine_types
(
	id int auto_increment
		primary key,
	name varchar(30) not null,
	constraint engine_type_name_uindex
		unique (name)
);

create or replace table maintenance_status
(
	id int auto_increment
		primary key,
	name varchar(30) not null,
	constraint maintenance_status_name_uindex
		unique (name)
);

create or replace table manufacturers
(
	id int auto_increment
		primary key,
	name varchar(30) not null,
	constraint manufacturers_name_uindex
		unique (name)
);

create or replace table models
(
	id int auto_increment
		primary key,
	name varchar(30) not null,
	manufacturer_id int not null,
	constraint models_name_uindex
		unique (name),
	constraint models_manufacturers_fk
		foreign key (manufacturer_id) references manufacturers (id)
);

create or replace table roles
(
	id int auto_increment
		primary key,
	name varchar(20) not null,
	constraint roles_name_uindex
		unique (name)
);

create or replace table services
(
	id int auto_increment
		primary key,
	name varchar(100) not null,
	price_per_hour double not null,
	enabled tinyint(1) default 1 not null,
	constraint services_name_uindex
		unique (name)
);

create or replace table users
(
	id int auto_increment
		primary key,
	email varchar(100) not null,
	password varchar(64) not null,
	first_name varchar(20) not null,
	last_name varchar(20) not null,
	phone varchar(10) not null,
	role_id int not null,
	enabled tinyint(1) default 1 not null,
	constraint user_credentials_email_uindex
		unique (email),
	constraint users_roles_fk
		foreign key (role_id) references roles (id)
);

create or replace table vehicle_categories
(
	id int auto_increment
		primary key,
	name varchar(30) not null,
	constraint car_categories_name_uindex
		unique (name)
);

create or replace table vehicles
(
	id int auto_increment
		primary key,
	customer_id int not null,
	registration_plate varchar(8) not null,
	identification_number varchar(17) not null,
	model_id int not null,
	category_id int not null,
	colour varchar(20) not null,
	engine_type_id int not null,
	enabled tinyint(1) not null,
	constraint cars_identification_number_uindex
		unique (identification_number),
	constraint cars_registration_plate_uindex
		unique (registration_plate),
	constraint cars_car_categories_fk
		foreign key (category_id) references vehicle_categories (id),
	constraint cars_engine_types_fk
		foreign key (engine_type_id) references engine_types (id),
	constraint cars_models_fk
		foreign key (model_id) references models (id),
	constraint vehicles_users_fk
		foreign key (customer_id) references users (id)
);

create or replace table maintenances
(
	id int auto_increment
		primary key,
	vehicle_id int not null,
	from_date date null,
	to_date date null,
	status_id int not null,
	constraint maintenances_maintenance_statuses_fk
		foreign key (status_id) references maintenance_status (id),
	constraint maintenances_vehicles_fk
		foreign key (vehicle_id) references vehicles (id)
);

create or replace table maintenance_services
(
	id int auto_increment
		primary key,
	maintenance_id int not null,
	service_id int not null,
	hours int null,
	constraint maintenance_services_maintenances_fk
		foreign key (maintenance_id) references maintenances (id),
	constraint maintenance_services_services_fk
		foreign key (service_id) references services (id)
);


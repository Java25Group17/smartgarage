INSERT INTO `currency` (`id`, `name`, `abbreviation`, `default_exchange_course`)
VALUES (1, 'Български лева', 'BGN', 1),
       (2, 'Британска лира', 'GBP', 2.29533),
       (3, 'Щатски долар', 'USD', 1.66809),
       (4, 'Швейцарски франк', 'CHF', 1.76678),
       (5, 'Евро', 'EUR', 1.95583);

INSERT INTO `currency_current_date` (`id`, `date`)
VALUES (1, '2021-04-09');

INSERT INTO `engine_types` (`id`, `name`)
VALUES (1, 'Бензинов'),
       (2, 'Дизелов'),
       (4, 'Електрически'),
       (3, 'Хибриден');

INSERT INTO `maintenance_status` (`id`, `name`)
VALUES (2, 'In progress'),
       (1, 'Not started'),
       (3, 'Ready for pickup');

INSERT INTO `manufacturers` (`id`, `name`)
VALUES (1, 'Opel'),
       (2, 'Ford'),
       (3, 'BMW'),
       (4, 'Audi'),
       (5, 'Alfa Romeo'),
       (6, 'KIA'),
       (7, 'Chevrolet'),
       (8, 'Citroen'),
       (9, 'Dacia'),
       (10, 'Mazda'),
       (11, 'Honda'),
       (12, 'VW'),
       (13, 'Peugeot'),
       (14, 'Hyundai');

INSERT INTO `models` (`id`, `name`, `manufacturer_id`)
VALUES (1, 'Corsa', 1),
       (2, 'Rio', 6),
       (3, 'Giulietta', 5),
       (4, '323F', 10);

INSERT INTO `roles` (`id`, `name`)
VALUES (1, 'Admin'),
       (3, 'Customer'),
       (2, 'Employee');

INSERT INTO `services` (`id`, `name`, `price_per_hour`, `enabled`)
VALUES (1, 'Repair of chassis parts', 15.5, 1),
       (2, 'Repair of a gear box', 15.5, 1),
       (3, 'Gear box oil replacement', 15.5, 1),
       (4, 'Engine repair', 15.5, 1),
       (5, 'Engine oil and filters replacement', 15.5, 1),
       (6, 'Replacement / Repair of hinges', 15.5, 1),
       (7, 'Replacement / Repair of billiards', 15.5, 1),
       (8, 'Replacement / Repair of steering, supports', 15.5, 1),
       (9, 'Front and rear axle adjustment', 15.5, 1),
       (10, 'Electrical services', 15.5, 1),
       (11, 'Computer diagnostics and error clearing', 15.5, 1),
       (12, 'Turbo Repair/Replacement for Diesel engines', 15.5, 1),
       (13, 'Repair/Replacement of Fuel Injectors for Benzin', 15.5, 1),
       (14, 'Repair/Replacement of Fuel Injectors Diesel cars', 15.5, 1),
       (15, 'Replacement of spark plugs', 15.5, 1),
       (16, 'Repair of refrigeration system, air conditioning', 15.5, 1),
       (17, 'Freon in AC', 15.5, 1),
       (18, 'Replacement of discs and disc pads', 15.5, 1),
       (19, 'Brakes Repair', 15.5, 1),
       (20, 'Tires replacement', 15.5, 1),
       (21, 'Straightening wheels', 15.5, 1),
       (22, 'Mounting, disassembly', 15.5, 1),
       (23, 'Balance', 15.5, 1),
       (24, 'Other car repair services upon request', 15.5, 1);

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`, `phone`, `role_id`, `enabled`)
VALUES (1, '_krasimira.v.mihaylova@gmail.com', '12345678', 'Krasimira', 'Mihaylova', ' ', 2, 1),
       (2, '_tsr74@abv.bg', 'pass1234', 'Tanya', 'Radeva', ' ', 3, 1),
       (3, 'krassy_v@abv.bg', '12345678', 'Krasi', 'Mihaylova', ' ', 3, 1),
       (4, 'tanya.radeva.a25@learn.telerikacademy.com', 'bd94dcda26fccb4e68d6a31f9b5aac0b571ae266d822620e901ef7ebe3a11d4f', 'Tanche', 'Radeva', ' ', 1, 1),
       (5, 'tsr74@abv.bg', '2cf32a7f9b6cd2b0454cd5b5e9b820fa079a1259e9130736c0145f2d7a5964ae',
        'Tanya', 'Radeva', '0897123456', 3, 1),
       (6, 'krasimira.v.mihaylova@gmail.com', 'd0d9dc6c90694f9e885d062a87899cfe63e4c6bdbe993f43b39d3200fcc93c7b',
        'Krasimira', 'Mihaylova', ' ', 2, 1);


INSERT INTO `vehicle_categories` (`id`, `name`)
VALUES (3, 'Bus'),
       (1, 'Car'),
       (2, 'Jeep'),
       (6, 'Motorcycle'),
       (5, 'Truck'),
       (4, 'Van');


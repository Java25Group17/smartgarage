# SmartGarage

Project Presentation
https://prezi.com/p/n-gocm6fj4yk/open-share/?utm_source=prezi&utm_medium=email&utm_content=Prezi&utm_campaign=Next_Share_A_Prezi_v2&UID=304126264

Trello Board
https://trello.com/b/iafhsIeR

Link to swagger documentation
http://localhost:8080/swagger-ui.html#/
![Rest functionality](/documents/img/swagger_view.png)
)


In order to run the project you should follow the next steps:
1. Install JDK 11
2. Install InteliJ Idea
3. Install MariaDB
4. Configure InteliJ Idea to use JDK 11 and MariaDB
5. Create database **smart_garage** using smart_garage_create.sql
6. Fill the database with test data using smart_garage_insert.sql
7. Create database user with all rights to smart_garage database, using userName: **smartGarageUser** and password: **smartGarageUser**
8. Open the project in InteliJ Idea.
9. Build and Run DeliveryItApplication project
10. Have fun!


![Database schema](/documents/smart_garage.png)

For application functionality see ![Application description](/documents/SmartGarage.pdf)
Anonymous
[index page](/documents/img/index_anonymous.png)
[Login page](/documents/img/login.png)
[Contact form](/documents/img/contact_form.png)
[Contact mail](/documents/img/contact_mail.png)
[Forgotten password](/documents/img/customer_forgotten_password.png)
[Forgotten password Email](/documents/img/customer_reset_password.png)


Client Views
[Customer visits](/documents/img/customer_visits.png)
[Customer Invoice](/documents/img/customer_pdf.png)
[Customer view service prices](documents/img/customer_services.png)

Employee Views
[Employee view customers](/documents/img/employee_customers.png)
[Employee view vehicles](/documents/img/employee_vehicles.png)
[Employee create vehicle](/documents/img/employee_create_vehicle.png)
[Employee create/update visit](/documents/img/employee_update_maintenance.png)


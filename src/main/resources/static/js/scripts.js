/*!
    * Start Bootstrap - Agency v6.0.3 (https://startbootstrap.com/theme/agency)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-agency/blob/master/LICENSE)
    */
    (function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
                this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate(
                    {
                        scrollTop: target.offset().top - 72,
                    },
                    1000,
                    "easeInOutExpo"
                );
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $(".js-scroll-trigger").click(function () {
        $(".navbar-collapse").collapse("hide");
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $("body").scrollspy({
        target: "#mainNav",
        offset: 74,
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);

//------------------------------------------------
// AJAX section
        // Getting vehicle models
        var $car_combo = $('#dropdownVehiclesButton');
        $car_combo.on('change', function(e) {
            $("#dropdownModelButton").html('<option value="" hidden="hidden">Loading...</option>');
            var elmt = document.getElementById("dropdownVehiclesButton");
            var elmtval = elmt.options[elmt.selectedIndex];
            var model = elmtval.value;

            $.ajax({
                type: "GET",
                url: "http://localhost:8080/api/manufacturers/" + model + "/models/" ,
                data: {},
                success: function (data) {
                    var s = '<option value="-1" hidden="hidden">Choose here</option>';
                    for (var i = 0; i < data.length; i++) {
                        s += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }
                    $("#dropdownModelButton").html(s);
                },
                error: function(xhr, status, err) {
                    alert("ajax status: " + status);
                    alert("ajax err: " + err);
                }
            });
        })

//------------------------------------------------
        // Sending User search form
        var $form = $('#userSearchForm');
        $form.on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: $form.attr('action'),
                type: 'post',
                data: $form.serialize(),
                success: function(response) {
                    var $user_data = $('#userTableData');
                    $user_data.html('');
                    $user_data.html(response);
                },
                error: function(xhr, status, err) {
                    alert("ajax status: " + status);
                    alert("ajax err: " + err);
                }
            });
        })
//------------------------------------------------
        // Sending Vehicles search form
        var $formVehicles = $('#vehicleSearchForm');
        $formVehicles.on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: $formVehicles.attr('action'),
                type: 'post',
                data: $formVehicles.serialize(),
                success: function(response) {
                    var $vehicle_data = $('#vehicleTableData');
                    $vehicle_data.html('');
                    $vehicle_data.html(response);
                },
                error: function(xhr, status, err) {
                    alert("ajax status: " + status);
                    alert("ajax err: " + err);
                }
            });
        })
//------------------------------------------------
        // Sending Vehicles add form
        var $formAddVehicle = $('#vehicleAddForm');
        $formAddVehicle.on('submit', function(e) {
            e.preventDefault();
            var elmt = document.getElementById("dropdowncustomerName");
            var userid = elmt.value;
            $.ajax({
                url: "http://localhost:8080/vehicles/create",
                type: 'post',
                data: $formAddVehicle.serialize(),
                success: function(response) {
                    //alert(response);
                    // if (response == "nok") {
                    //     //window.location.href = "http://localhost:8080/vehicles/create/" + userid;
                    //     window.location.href = "http://localhost:8080/error";
                    // } else {
                    //     window.location.href = "http://localhost:8080/vehicles";
                    // }
                    window.location.href = "http://localhost:8080/vehicles";
                },
                error: function(xhr, status, err) {
                    //alert("ajax status: " + status);
                    //alert("ajax err: " + err);
                    //window.location.href = "http://localhost:8080/error";
                    //window.location.href = "http://localhost:8080/vehicles/create/" + userid;
                    window.location.href = "http://localhost:8080/vehicles";
                }
            });
        })
})(jQuery); // End of use strict

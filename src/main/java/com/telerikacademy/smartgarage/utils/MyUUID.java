package com.telerikacademy.smartgarage.utils;

import java.util.UUID;

public class MyUUID {
    public String getUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}

package com.telerikacademy.smartgarage.utils;

import com.telerikacademy.smartgarage.models.Currency;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

public class ExchangeRates_DOM {
    private Set<String> currencyNames = new HashSet<>();
    private Map<String, Double> exchangeRate;
    private String url = "https://www.bnb.bg/Statistics/StExternalSector/StExchangeRates/StERForeignCurrencies/index.htm?download=xml&search=&lang=BG";
    private StringBuffer response = new StringBuffer();

    public ExchangeRates_DOM(List<Currency> currencies) {
        exchangeRate = new HashMap<>();

        for (int i = 0; i < currencies.size(); i++) {
            currencyNames.add(currencies.get(i).getAbbreviation());
        }
    }

    public Map<String, Double> getExchangeRates() {
            try(InputStream is = new URL(url).openStream();) {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(is);

                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("ROW");

                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        String code = eElement.getElementsByTagName("CODE").item(0).getTextContent();
                        if (currencyNames.contains(code)) {
                            String mapKey = new String(code);

                            String rate = eElement.getElementsByTagName("RATE").item(0).getTextContent();
                            Double doubleRate = Double.parseDouble(rate);
                            Double mapValue = doubleRate;

                            exchangeRate.put(mapKey, mapValue);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        return exchangeRate;
    }
}

package com.telerikacademy.smartgarage.utils;

public class Constants {
    public static final String NAME_CANT_BE_EMPTY = "Name can't be empty.";
    public static final String UNAUTHORISED = "Unauthorised!";
}

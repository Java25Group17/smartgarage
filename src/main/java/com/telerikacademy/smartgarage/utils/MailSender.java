package com.telerikacademy.smartgarage.utils;

import com.telerikacademy.smartgarage.models.User;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class MailSender {
    private final String username = "smart.garage.g17@gmail.com";
    private final String password = "telerik@25";
    Properties prop;
    private String e_trace;

    public MailSender() {
        prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS
    }

    public String getErrorTrace() {
        return e_trace;
    }

    private boolean mainSendMail(String toMail,
                                 String subject,
                                 String body,
                                 String fileToAttach) {
        boolean result = true;

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(toMail)
            );
            message.setSubject(subject);

            if (!fileToAttach.isEmpty()) {
                MimeBodyPart mimeBodyPart = new MimeBodyPart();
                mimeBodyPart.setText(body);

                Multipart multipart = new MimeMultipart();
                multipart.addBodyPart(mimeBodyPart);

                MimeBodyPart attachmentBodyPart = new MimeBodyPart();
                attachmentBodyPart.attachFile(new File(fileToAttach));
                multipart.addBodyPart(attachmentBodyPart);

                message.setContent(multipart);
            } else {
                message.setText(body);
            }

            Transport.send(message);

//            System.out.println("Done");

        } catch (MessagingException | IOException e) {
            //e.printStackTrace();
            e_trace = new String(e.getMessage());
            result = false;
        }

        return result;
    }

    public boolean sendMail(String toMail, String subject, String body, String fileToAttach) {
        return mainSendMail(toMail,
                subject,
                body,
                fileToAttach);
    }

    public boolean sendMail(String toMail, String subject, String pass) {
        return mainSendMail(toMail,
                subject,
                createNewPasswordBody(pass),
                "");
    }

    public  boolean sendMail(String subject, String mess) {
        return mainSendMail(username,
                subject,
                mess,
                "");
    }

    public boolean sendMail(User user, String subject, String linkForPasswordReset) {
        return mainSendMail(user.getEmail(),
                subject,
                resetPasswordBody(user, linkForPasswordReset),
                "");
    }

    private String createNewPasswordBody(String password) {
        StringBuilder mailBody = new StringBuilder();
        mailBody.append("Здравейте,");
        mailBody.append(System.lineSeparator());
        mailBody.append(System.lineSeparator());
        mailBody.append("Паролата за нашата система е ");
        mailBody.append(password);
        mailBody.append(System.lineSeparator());
        mailBody.append("Желателно е при първо влизане в системата да я промените!");
        mailBody.append(System.lineSeparator());
        mailBody.append(System.lineSeparator());
        mailBody.append("Поздрави от екипа на SmartGarage.G17!");

        return mailBody.toString();
    }

    private String resetPasswordBody(User user, String linkForPasswordReset) {
        StringBuilder mailBody = new StringBuilder();
        mailBody.append("Здравейте, ");
        mailBody.append(user.getFirstName());
        mailBody.append(" ");
        mailBody.append(user.getLastName());
        mailBody.append(System.lineSeparator());
        mailBody.append(System.lineSeparator());
        mailBody.append("Получихме заявка за ресетване на вашата парола.");
        mailBody.append(System.lineSeparator());
        mailBody.append("Можете да я промените като последвате линка по-долу.");
        mailBody.append(System.lineSeparator());
        mailBody.append("След използването на линка ще се появи форма, в която да въведете новата си парола.");
        mailBody.append(System.lineSeparator());
        mailBody.append(System.lineSeparator());
        mailBody.append("ВНИМАНИЕ!!! Линкът ще бъде активен в следващите 24 часа от получаване на този мейл");
        mailBody.append(System.lineSeparator());
        mailBody.append(System.lineSeparator());
        mailBody.append(linkForPasswordReset);
        mailBody.append(System.lineSeparator());
        mailBody.append(System.lineSeparator());
        mailBody.append("Поздрави от екипа на SmartGarage.G17!");

        return mailBody.toString();
    }
}
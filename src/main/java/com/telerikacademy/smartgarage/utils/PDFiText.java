package com.telerikacademy.smartgarage.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.Maintenance;
import com.telerikacademy.smartgarage.models.MaintenanceServiceItem;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Stream;

public class PDFiText {
    private final Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK);
    private final Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private final Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

    public Document createPDF(List<Maintenance> maintenances, Currency currency) throws IllegalEntityException{
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("src/main/resources/pdf/pdfInvoice.pdf"));
            document.open();
            for (Maintenance maintenance : maintenances) {
                generateInvoice(document, maintenance, currency);
                document.newPage();
            }
            document.close();
            return document;
        } catch (DocumentException | IOException | URISyntaxException e) {
            e.printStackTrace();
            throw new IllegalEntityException("Problem with PDF");
        }
    }

    private void generateInvoice(Document document, Maintenance maintenance, Currency currency) throws DocumentException, IOException, URISyntaxException {
        Paragraph preface = new Paragraph();

        var vehicle = maintenance.getVehicle();
        var user = vehicle.getCustomer();
        var fromDate = maintenance.getFromDate();
        var toDate = maintenance.getToDate();
        preface.add(new Paragraph(String.format("Invoice # %d / %s - %s"
                , maintenance.getId(),
                fromDate == null ? "Not started" : fromDate.toString(),
                toDate == null ? "Not finished" : toDate.toString()), catFont));

        addEmptyLine(preface, 1);
        document.add(new LineSeparator());
        addEmptyLine(preface, 1);
        preface.add(new Paragraph(
                String.format("Customer: %s %s ", user.getFirstName(), user.getLastName()), smallBold));
        preface.add(new Paragraph(
                String.format("Phone: %s ", user.getPhone()), smallBold));
        addEmptyLine(preface, 2);
        var model = vehicle.getModel();
        preface.add(new Paragraph(
                String.format("Vehicle  %s, %s/%s ", vehicle.getCategory().getName(), model.getManufacturer().getName(), model.getName()),
                smallBold));
        preface.add(new Paragraph(
                String.format("Registration plate:  %s ", vehicle.getRegistrationPlate()),
                smallBold));

        addEmptyLine(preface, 1);
        preface.add(new Paragraph(
                String.format("Status:  %s ", maintenance.getMaintenanceStatus().getName()),
                smallBold));
        addEmptyLine(preface, 2);

        document.add(preface);

        PdfPTable table = new PdfPTable(new float[]{8, 2, 1, 2});
        table.setTotalWidth(520);
        table.setLockedWidth(true);

        addTableHeader(table, currency.getAbbreviation());
        double total = 0;
        double currencyExchangeCourse = currency.getDefaultExchangeCourse();
        DecimalFormat df = new DecimalFormat("#0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        for (MaintenanceServiceItem serviceItem :
                maintenance.getMaintenanceServices()) {
            double pricePerHour = serviceItem.getService().getPricePerHour() / currencyExchangeCourse;
            var hour = serviceItem.getHours();
            var pricePerService = pricePerHour * hour;

            total += pricePerService;
            addRow(table,
                    serviceItem.getService().getName(),
                    df.format(pricePerHour),
                    String.valueOf(hour),
                    df.format(pricePerService));
        }
        addRow(table, "Total", "", "", df.format(total));
        document.add(table);
    }

    private void addTableHeader(PdfPTable table, String currency) {
        Stream.of("Services", String.format("Price/hour (%s)", currency), "Hours", "Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addRow(PdfPTable table, String serviceName, String pricePerHour, String hours, String priceForService) {
        var cell = new PdfPCell(new Phrase(serviceName));

        table.addCell(cell);
        cell = new PdfPCell(new Phrase(pricePerHour));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(hours));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(priceForService));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.addCell(cell);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}

package com.telerikacademy.smartgarage.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Arrays;


@Configuration
public class SpringFoxConfig {

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build().apiInfo(apiInfo())
            .securityContexts(Arrays.asList(securityContext()))
            .securitySchemes(Arrays.asList(basicScheme()));
}

    private ApiInfo apiInfo() {

        return new ApiInfoBuilder()
                .title("Smart Garage")
                .description("Smart Garage Api Documentation")

                .build();
    }
    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(Arrays.asList(basicReference()))
                .build();
    }

    private SecurityScheme basicScheme() {
        return new BasicAuth("basicAuth");
    }

    private SecurityReference basicReference() {
        return new SecurityReference("basicAuth", new AuthorizationScope[0]);
    }
}


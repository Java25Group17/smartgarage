package com.telerikacademy.smartgarage;

import com.telerikacademy.smartgarage.services.contracts.CurrencyService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SmartGarageApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(SmartGarageApplication.class, args);
        CurrencyService currencyService = applicationContext.getBean(CurrencyService.class);
        currencyService.update();
    }
}

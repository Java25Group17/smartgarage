package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.CurrentDate;
import com.telerikacademy.smartgarage.repositories.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CurrencyRepositoryImpl extends AbstractGenericGetRepository<Currency> implements CurrencyRepository {

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Currency> getAll() {
        return super.getAll(Currency.class);
    }

    @Override
    public CurrentDate getDate() {
        try (Session session = sessionFactory.openSession()) {
            Query<CurrentDate> query = session.createQuery(
                    "select c from CurrentDate c ", CurrentDate.class);
            return query.list().get(0);
        }
    }

    @Override
    public void updateDate(CurrentDate currentDate) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(currentDate);
            session.getTransaction().commit();
        }
    }

    @Override
    public Currency getById(Integer id) {
        return super.getByField("id", id, Currency.class);
    }

    @Override
    public Currency getByAbbreviation(String abbr) {
        return super.getByField("abbreviation", abbr, Currency.class);
    }

    @Override
    public Currency update(Currency currency) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(currency);
            session.getTransaction().commit();
        }
        return currency;
    }
}

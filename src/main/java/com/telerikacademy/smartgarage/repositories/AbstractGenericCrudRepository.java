package com.telerikacademy.smartgarage.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class AbstractGenericCrudRepository<T> extends AbstractGenericGetRepository<T> {
    SessionFactory sessionFactory;

    public AbstractGenericCrudRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    public T create(T objectToCreate) {
        try (Session session = sessionFactory.openSession()) {
            session.save(objectToCreate);
            return objectToCreate;
        }
    }

    public T update(T objectToUpdate) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(objectToUpdate);
            session.getTransaction().commit();
            return objectToUpdate;
        }
    }
}

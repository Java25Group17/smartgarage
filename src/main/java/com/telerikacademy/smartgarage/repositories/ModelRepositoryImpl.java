package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Model;
import com.telerikacademy.smartgarage.models.ModelSearchParameters;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ModelRepositoryImpl extends AbstractGenericCrudRepository<Model> implements ModelRepository {
    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Model> getAll() {
        return super.getAll(Model.class);
    }

    @Override
    public List<Model> getAll(ModelSearchParameters parameters) {
        try (Session session = sessionFactory.openSession()) {
            String queryStr = "from Model ";
            var filters = new ArrayList<String>();
            if (parameters.getName() != null) {
                filters.add(" lower(name) like concat('%',:name,'%') ");
            }
            if (parameters.getManufacturerId() != null) {
                filters.add(" manufacturer.id = :manufacturerId");
            }
            if (filters.size() > 0) {
                var finalQuery = String.join(" and ", filters);
                queryStr = queryStr + " where " + finalQuery;
            }
            Query<Model> query = session.createQuery(queryStr, Model.class);
            var name = parameters.getName();
            if (name!= null) {
                query.setParameter("name",name);
            }
            var manufacturerId = parameters.getManufacturerId();
            if(manufacturerId!= null){
                query.setParameter("manufacturerId", manufacturerId);
            }
            return query.list();
        }
    }

    @Override
    public Model getById(Integer id) {
        return super.getByField("id", id, Model.class);
    }

   @Override
    public Model getByName(String name) {
        return super.getByField("name", name, Model.class);
    }

    @Override
    public List<Model> getByManufacturer(Integer manufacturerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery("from Model where manufacturer.id = :id", Model.class);
            query.setParameter("id", manufacturerId);
            return query.list();
        }
    }

    @Override
    public Model create(Model objectToCreate) {
        return super.create(objectToCreate);
    }

    @Override
    public Model update(Model objectToUpdate) {
        return super.update(objectToUpdate);
    }
}

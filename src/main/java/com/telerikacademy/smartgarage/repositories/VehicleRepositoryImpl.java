package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.VehicleSearchParameters;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class VehicleRepositoryImpl extends AbstractGenericCrudRepository<Vehicle> implements VehicleRepository {

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Vehicle> getAll(VehicleSearchParameters parameters) {
        try (Session session = sessionFactory.openSession()) {
            String queryStr = "from Vehicle ";
            var filters = new ArrayList<String>();
            if (parameters.isEnabled()) {
                filters.add(" enabled = true");
            }
            if (parameters.getCustomerId() != null) {
                filters.add(" customer.id = :customerId ");
            }
            if (parameters.getModelId() != null) {
                filters.add(" model.id = :modelId ");
            }
            if (parameters.getManufacturerId() != null) {
                filters.add(" model.manufacturer.id = :manufacturerId ");
            }
            if (parameters.getRegistrationNumber() != null) {
                filters.add(" registrationPlate like concat('%', :registrationNumber, '%') ");
            }
            if (filters.size() > 0) {
                var finalQuery = String.join(" and ", filters);
                queryStr = queryStr + " where " + finalQuery;
            }
            Query<Vehicle> query = session.createQuery(queryStr, Vehicle.class);
            var customerId = parameters.getCustomerId();
            if (customerId != null) {
                query.setParameter("customerId", customerId);
            }
            var modelId = parameters.getModelId();
            if (modelId != null) {
                query.setParameter("modelId", modelId);
            }
            var manufacturerId = parameters.getManufacturerId();
            if (manufacturerId != null) {
                query.setParameter("manufacturerId", manufacturerId);
            }
            var registrationNumber = parameters.getRegistrationNumber();
            if (registrationNumber != null) {
                query.setParameter("registrationNumber", registrationNumber);
            }
            return query.list();
        }
    }

    @Override
    public Vehicle getById(Integer id) {
        return super.getByField("id", id, Vehicle.class);
    }

    @Override
    public Vehicle getByIdentificationNumber(String number) {
        return super.getByField("identificationNumber", number, Vehicle.class);
    }

    @Override
    public Vehicle create(Vehicle objectToCreate) {
        return super.create(objectToCreate);
    }

    @Override
    public Vehicle update(Vehicle objectToUpdate) {
        return super.update(objectToUpdate);
    }

    @Override
    public void disableVehicles(Integer customerId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            String hqlUpdate = "update Vehicle v set v.enabled = false  where v.customer.id = :customerId";

            int updatedEntities = session.createQuery(hqlUpdate).
                    setParameter("customerId", customerId).
                    executeUpdate();
            System.out.println("updatedEntities: " + updatedEntities);
            session.getTransaction().commit();
        }
    }
}



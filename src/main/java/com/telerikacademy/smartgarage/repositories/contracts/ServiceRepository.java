package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.ServiceSearchParameters;

import java.util.List;

public interface ServiceRepository {
    List<Service> getAll(ServiceSearchParameters parameters);

    Service getById(Integer id);

    Service create(Service objectToCreate);

    Service update(Service objectToUpdate);

    Service getByName(String name);
}

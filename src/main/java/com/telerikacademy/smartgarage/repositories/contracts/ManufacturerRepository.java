package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Manufacturer;
import com.telerikacademy.smartgarage.models.ManufacturerSearchParameters;

import java.util.List;

public interface ManufacturerRepository {
    List<Manufacturer> getAll();

    Manufacturer getById(Integer id);

    Manufacturer create(Manufacturer manufacturer);

    Manufacturer update(Manufacturer manufacturer);

    List<Manufacturer> getAll(ManufacturerSearchParameters parameters);

    Manufacturer getByName(String name);
}

package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.EngineType;

import java.util.List;

public interface EngineTypeRepository {
    List<EngineType> getAll();

    EngineType getById(Integer id);

}

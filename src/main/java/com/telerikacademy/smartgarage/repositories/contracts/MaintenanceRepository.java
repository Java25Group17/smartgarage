package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Maintenance;
import com.telerikacademy.smartgarage.models.MaintenanceSearchParameters;
import com.telerikacademy.smartgarage.models.Vehicle;

import java.util.List;

public interface MaintenanceRepository {
    List<Maintenance> getAll(MaintenanceSearchParameters parameters);

    Maintenance getById(Integer id);

    Maintenance create(Maintenance objectToCreate);

    Maintenance update(Maintenance objectToUpdate);

    Long selectCountVehicleInMaintenance(Integer vehicleId);

    Long selectCountVehicleForClient(Integer customerId);

    Maintenance getByNotFinishedVehicle(Vehicle vehicle);
}

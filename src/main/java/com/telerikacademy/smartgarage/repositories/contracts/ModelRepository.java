package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Model;
import com.telerikacademy.smartgarage.models.ModelSearchParameters;

import java.util.List;

public interface ModelRepository {
    List<Model> getAll();

    List<Model> getAll(ModelSearchParameters parameters);

    Model getById(Integer id);

    List<Model> getByManufacturer(Integer manufacturerId);

    Model create(Model objectToCreate);

    Model update(Model objectToUpdate);

    Model getByName(String name);
}

package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.MaintenanceServiceItem;

public interface MaintenanceServiceRepository {
    Long selectCountServiceInUse(Integer id);

    MaintenanceServiceItem getItem(Integer maintenanceId, Integer serviceId);

    MaintenanceServiceItem getById(Integer id);
}

package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.MaintenanceStatus;

import java.util.List;

public interface MaintenanceStatusRepository {
    List<MaintenanceStatus> getAll();
    MaintenanceStatus getById(Integer id);
 }

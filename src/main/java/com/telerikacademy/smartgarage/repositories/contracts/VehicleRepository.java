package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.VehicleSearchParameters;

import java.util.List;

public interface VehicleRepository {
    List<Vehicle> getAll(VehicleSearchParameters parameters);

    Vehicle getById(Integer id);

    Vehicle create(Vehicle objectToCreate);

    Vehicle update(Vehicle objectToUpdate);

    Vehicle getByIdentificationNumber(String identificationNumber);

    void disableVehicles(Integer customerId);
}

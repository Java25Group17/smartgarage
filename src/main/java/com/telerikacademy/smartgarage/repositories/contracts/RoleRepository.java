package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getById(Integer id);

    Role getByName(String roleName);
}

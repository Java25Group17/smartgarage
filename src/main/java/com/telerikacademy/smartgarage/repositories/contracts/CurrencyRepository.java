package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.CurrentDate;

import java.util.List;

public interface CurrencyRepository {
    List<Currency> getAll();

    Currency getById(Integer id);

    Currency getByAbbreviation(String abbr);

    CurrentDate getDate();

    void updateDate(CurrentDate currentDate);

    Currency update (Currency currency);
}

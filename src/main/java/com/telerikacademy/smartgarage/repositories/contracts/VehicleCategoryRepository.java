package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.VehicleCategory;

import java.util.List;

public interface VehicleCategoryRepository {

    List<VehicleCategory> getAll();

    VehicleCategory getById(Integer id);

    VehicleCategory create(VehicleCategory objectToCreate);

    VehicleCategory update(VehicleCategory objectToUpdate);

    VehicleCategory getByName(String name);
}

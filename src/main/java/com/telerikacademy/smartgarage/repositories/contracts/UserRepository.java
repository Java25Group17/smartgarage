package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.UserSearchParameters;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getById(int id);

    User getByEmail(String email);

    User create(User user);

    User update(User user);

    List<User> filterByMultiple(UserSearchParameters searchParameters);
}

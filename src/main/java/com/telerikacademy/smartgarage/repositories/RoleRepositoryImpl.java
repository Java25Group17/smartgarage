package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.repositories.contracts.RoleRepository;
import com.telerikacademy.smartgarage.models.Role;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl extends AbstractGenericGetRepository<Role> implements RoleRepository {

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Role> getAll() {
        return getAll(Role.class);
    }

    @Override
    public Role getById(Integer id) {
        return getByField("id", id, Role.class);
    }

    @Override
    public Role getByName(String roleName) {
        return getByField("name", roleName, Role.class);
    }
}

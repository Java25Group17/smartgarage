package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.ServiceSearchParameters;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ServiceRepositoryImpl extends AbstractGenericCrudRepository<Service> implements ServiceRepository {

    @Autowired
    public ServiceRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Service> getAll(ServiceSearchParameters parameters) {
        try (Session session = sessionFactory.openSession()) {
            String queryStr = "from Service  ";
            var filters = new ArrayList<String>();
            if (parameters.isEnabled()) {
                filters.add(" enabled = true");
            }

            if (parameters.getName() != null) {
                filters.add(" lower(name) like concat('%', :name, '%') ");
            }
            if(parameters.getMinPrice()!= null){
                filters.add(" pricePerHour >= :minPrice ");
            }
            if(parameters.getMaxPrice()!= null){
                filters.add(" pricePerHour <= :maxPrice ");
            }
            if (filters.size() > 0) {
                var finalQuery = String.join(" and ", filters);
                queryStr = queryStr + " where " + finalQuery;
            }
            Query<Service> query = session.createQuery(queryStr, Service.class);
            var name = parameters.getName();
            if (name != null) {
                query.setParameter("name", parameters.getName());
            }
            if(parameters.getMinPrice()!= null){
                query.setParameter("minPrice", parameters.getMinPrice());
            }
            if(parameters.getMaxPrice()!= null){
                query.setParameter("maxPrice", parameters.getMaxPrice());
            }
            return query.list();
        }
    }

    @Override
    public Service getById(Integer id) {
        return super.getByField("id", id, Service.class);
    }

    @Override
    public Service getByName(String name) {
        return super.getByField("name", name, Service.class);
    }

    @Override
    public Service create(Service objectToCreate) {
        return super.create(objectToCreate);
    }

    @Override
    public Service update(Service objectToUpdate) {
        return super.update(objectToUpdate);
    }
}

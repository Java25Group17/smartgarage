package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.EngineType;
import com.telerikacademy.smartgarage.repositories.contracts.EngineTypeRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EngineTypeRepositoryImpl extends AbstractGenericGetRepository<EngineType> implements EngineTypeRepository {
    @Autowired
    public EngineTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<EngineType> getAll() {
        return super.getAll(EngineType.class);
    }

    @Override
    public EngineType getById(Integer id) {
        return super.getByField("id", id, EngineType.class);
    }
}

package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.MaintenanceStatus;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceStatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MaintenanceStatusRepositoryImpl extends AbstractGenericGetRepository<MaintenanceStatus> implements MaintenanceStatusRepository {
    @Autowired
    public MaintenanceStatusRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<MaintenanceStatus> getAll() {
        return super.getAll(MaintenanceStatus.class);
    }

    @Override
    public MaintenanceStatus getById(Integer id) {
        return super.getByField("id", id, MaintenanceStatus.class);
    }
}

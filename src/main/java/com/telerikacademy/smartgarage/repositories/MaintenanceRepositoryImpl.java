package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Maintenance;
import com.telerikacademy.smartgarage.models.MaintenanceSearchParameters;
import com.telerikacademy.smartgarage.models.MaintenanceStatus;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MaintenanceRepositoryImpl
        extends AbstractGenericCrudRepository<Maintenance>
        implements MaintenanceRepository {
    @Autowired
    public MaintenanceRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Maintenance> getAll(MaintenanceSearchParameters parameters) {
        try (Session session = sessionFactory.openSession()) {
            String queryStr = "select distinct m from Maintenance m";
            var filters = new ArrayList<String>();
            var customerId = parameters.getCustomerId();
            var vehicleId = parameters.getVehicleId();
            var statusId = parameters.getStatusId();

            if (parameters.getStatusId() != null && parameters.getStatusId() > 0) {
                filters.add(" maintenanceStatus.id = :statusId ");
            }
            if (customerId != null && customerId > 0) {
                filters.add(" vehicle.customer.id = :customerId ");
            }
            if (vehicleId != null && vehicleId > 0) {
                filters.add(" vehicle.id = :vehicleId ");
            }
            var fromDate = parameters.getFromDate();
            var toDate = parameters.getToDate();
            StringBuilder dateInterval = new StringBuilder();
            if (toDate == null)
                dateInterval.append(" fromDate is null ");
            fromDate = fromDate == null ? LocalDate.EPOCH : parameters.getFromDate();
            toDate = toDate == null ? LocalDate.now() : parameters.getToDate();
            if (dateInterval.length() != 0) {
                dateInterval.append(" or ");
            }

            dateInterval.append(" fromDate between :startDate and :endDate ");
            dateInterval.append("or");
            dateInterval.append(" toDate between :startDate and :endDate ");
            dateInterval.insert(0, " (");
            dateInterval.append(") ");
            filters.add(dateInterval.toString());

            if (filters.size() > 0) {
                var finalQuery = String.join(" and ", filters);
                queryStr = queryStr + " where " + finalQuery;
            }
            queryStr += " ORDER BY fromDate ";
            if (parameters.getOrderByDate() != null) {
                queryStr += parameters.getOrderByDate();

            } else {
                queryStr += "asc";
            }
            Query<Maintenance> query = session.createQuery(queryStr, Maintenance.class);

            if (customerId != null) {
                query.setParameter("customerId", customerId);
            }
            if (vehicleId != null && vehicleId > 0) {
                query.setParameter("vehicleId", vehicleId);
            }
            if (statusId != null && statusId > 0) {
                query.setParameter("statusId", statusId);
            }
            query.setParameter("startDate", fromDate);
            query.setParameter("endDate", toDate);
            return query.list();
        }
    }

    @Override
    public Maintenance getById(Integer id) {
        return super.getByField("id", id, Maintenance.class);
    }

    @Override
    public Maintenance create(Maintenance objectToCreate) {
        return super.create(objectToCreate);
    }

    @Override
    public Maintenance update(Maintenance objectToUpdate) {
        return super.update(objectToUpdate);
    }

    @Override
    public Long selectCountVehicleInMaintenance(Integer vehicleId) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("select count(distinct m.id) from Maintenance m " +
                    "where m.vehicle.id = :vehicleId and NOT maintenanceStatus.id = :status group by m.id");
            query.setParameter("vehicleId", vehicleId);
            query.setParameter("status", MaintenanceStatus.READY_FOR_PICKUP);
            return (Long) query.uniqueResult();
        }
    }

    public Long selectCountVehicleForClient(Integer customerId) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("select count(distinct m.id) from Maintenance m " +
                    "where m.vehicle.customer.id = :customerId and NOT maintenanceStatus.id = :status group by m.id");
            query.setParameter("customerId", customerId);
            query.setParameter("status", MaintenanceStatus.READY_FOR_PICKUP);
            return (Long) query.uniqueResult();
        }
    }

    public Maintenance getByNotFinishedVehicle(Vehicle vehicle) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from Maintenance m " +
                    "where m.vehicle = :vehicle and NOT maintenanceStatus.id = :status", Maintenance.class);
            query.setParameter("vehicle", vehicle);
            query.setParameter("status", MaintenanceStatus.READY_FOR_PICKUP);
            List<Maintenance> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("Maintenance not found!");
            } else {
                return result.get(0);
            }
        }
    }
}

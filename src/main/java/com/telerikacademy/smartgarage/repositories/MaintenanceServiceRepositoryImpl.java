package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.MaintenanceServiceItem;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MaintenanceServiceRepositoryImpl implements MaintenanceServiceRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public MaintenanceServiceRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long selectCountServiceInUse(Integer serviceId) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("select count(distinct ms.id) from MaintenanceServiceItem ms " +
                    "where ms.service.id = :serviceId and NOT ms.maintenance.maintenanceStatus.id =3 group by ms.id");
            query.setParameter("serviceId", serviceId);
            return (Long) query.uniqueResult();
        }
    }
    public MaintenanceServiceItem getById(Integer id){
        try (Session session = sessionFactory.openSession()) {
            MaintenanceServiceItem item = session.get(MaintenanceServiceItem.class, id);
            if (item == null) {
                throw new EntityNotFoundException("ServiceItem", id);
            }
            return item;
        }
    }

    public MaintenanceServiceItem getItem(Integer maintenanceId, Integer serviceId) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from MaintenanceServiceItem ms " +
                    "where ms.service.id = :serviceId and  ms.maintenance.id = :maintenanceId", MaintenanceServiceItem.class);
            query.setParameter("serviceId", serviceId);
            query.setParameter("maintenanceId", maintenanceId);
            List<MaintenanceServiceItem> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("Maintenance not found!");
            } else {
                return result.get(0);
            }
        }
    }
}

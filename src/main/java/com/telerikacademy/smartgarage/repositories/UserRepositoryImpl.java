package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.UserSearchParameters;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private static final String CUSTOMER_ROLE = "Customer";

    private final SessionFactory sessionFactory;

    private final MaintenanceRepository maintenanceRepository;

    public UserRepositoryImpl(SessionFactory sessionFactory,
                              MaintenanceRepository maintenanceRepository) {
        this.sessionFactory = sessionFactory;
        this.maintenanceRepository = maintenanceRepository;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            String queryStr = "select u from User u where u.role.name = :roleName and u.enabled = true";
            Query<User> query = session.createQuery(queryStr, User.class);
            query.setParameter("roleName", CUSTOMER_ROLE);
            return query.list();
        }
    }

    @Override
    public List<User> filterByMultiple(UserSearchParameters searchParameters) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryStr = new StringBuilder();
            queryStr.append("select distinct m.vehicle.customer from Maintenance m where");

            LocalDate fromDate = searchParameters.getFromDate();
            if (fromDate == null)
                fromDate = LocalDate.EPOCH;

            LocalDate toDate = searchParameters.getToDate();
            if (toDate == null)
                toDate = LocalDate.now();

            queryStr.append(" (m.fromDate is null or (m.fromDate between :fromDate and :toDate)");
            queryStr.append(" or (:fromDate between m.fromDate and COALESCE(m.toDate,:toDate)))");

            queryStr.append(" and m.vehicle.customer.role.name = :roleName");

            if (searchParameters.isEnabled() == null) {
                queryStr.append(" and m.vehicle.customer.enabled = true");
            } else {
                if (searchParameters.isEnabled().equals("true")) {
                    queryStr.append(" and m.vehicle.customer.enabled = true");
                }
                if (searchParameters.isEnabled().equals("false")) {
                    queryStr.append(" and m.vehicle.customer.enabled = false");
                }
            }
            if (searchParameters.getFirstName() != null && !searchParameters.getFirstName().isBlank()) {
                queryStr.append(" and lcase(m.vehicle.customer.firstName) like concat('%',:firstName,'%') ");
            }
            if (searchParameters.getLastName() != null && !searchParameters.getLastName().isBlank()) {
                queryStr.append(" and lcase(m.vehicle.customer.lastName) like concat('%',:lastName,'%') ");
            }
            if (searchParameters.getEmail() != null && !searchParameters.getEmail().isBlank()) {
                queryStr.append(" and lcase(m.vehicle.customer.email) like concat('%',:email,'%') ");
            }
            if (searchParameters.getPhone() != null && !searchParameters.getPhone().isBlank()) {
                queryStr.append(" and m.vehicle.customer.phone like concat('%',:phone,'%') ");
            }
            if (searchParameters.getModel() != null
                    && !searchParameters.getModel().isBlank()
                    && !searchParameters.getModel().equals("-1")) {
                queryStr.append(" and m.vehicle.model.id = :model ");
            }
            if (searchParameters.getMark() != null
                    && !searchParameters.getMark().isBlank()
                    && !searchParameters.getMark().equals("-1")) {
                queryStr.append(" and m.vehicle.model.manufacturer.id = :mark ");
            }
            if (searchParameters.getOrderBy() != null
                    && !searchParameters.getOrderBy().isBlank()
                    && !searchParameters.getOrderBy().equals("-1")) {
                switch (searchParameters.getOrderBy()) {
                    case "name":
                        queryStr.append(" ORDER BY m.vehicle.customer.firstName ASC");
                        break;
                    case "date":
                        queryStr.append(" ORDER BY m.fromDate DESC, m.toDate");
                        break;
                }
            }
            Query<User> query = session.createQuery(queryStr.toString(), User.class);

            query.setParameter("fromDate", fromDate);
            query.setParameter("toDate", toDate);
            query.setParameter("roleName", CUSTOMER_ROLE);
            if (searchParameters.getFirstName() != null && !searchParameters.getFirstName().isBlank()) {
                query.setParameter("firstName", searchParameters.getFirstName().toLowerCase());
            }
            if (searchParameters.getLastName() != null && !searchParameters.getLastName().isBlank()) {
                query.setParameter("lastName", searchParameters.getLastName().toLowerCase());
            }
            if (searchParameters.getEmail() != null && !searchParameters.getEmail().isBlank()) {
                query.setParameter("email", searchParameters.getEmail().toLowerCase());
            }
            if (searchParameters.getPhone() != null && !searchParameters.getPhone().isBlank()) {
                query.setParameter("phone", searchParameters.getPhone());
            }
            if (searchParameters.getModel() != null
                    && !searchParameters.getModel().isBlank()
                    && !searchParameters.getModel().equals("-1")) {
                int val = Integer.parseInt(searchParameters.getModel());
                query.setParameter("model", val);
            }
            if (searchParameters.getMark() != null
                    && !searchParameters.getMark().isBlank()
                    && !searchParameters.getMark().equals("-1")) {
                int val = Integer.parseInt(searchParameters.getMark());
                query.setParameter("mark", val);
            }
            return query.list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "select u from User u " +
                            "where u.id = :id and u.enabled = true",
                    User.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", id);
            }
            return query.list().get(0);
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "select u from User u " +
                            "where u.email = :email and u.enabled = true",
                    User.class);
            query.setParameter("email", email);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return query.list().get(0);
        }
    }

    @Override
    public User create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public User update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
        return user;
    }
}

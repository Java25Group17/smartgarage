package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public abstract class AbstractGenericGetRepository<T> {

    SessionFactory sessionFactory;

    public AbstractGenericGetRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public <V> T getByField(String fieldName, V fieldValue, Class<T> clazz) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = String.format("from %s where %s = :value", clazz.getName(), fieldName);
            Query<T> query = session.createQuery(queryString, clazz);
            query.setParameter("value", fieldValue);
            return query.uniqueResultOptional().
                    orElseThrow(() -> new EntityNotFoundException(
                            String.format("%s with %s %s not found",
                                    clazz.getSimpleName(),
                                    fieldName,
                                    fieldValue)));
        }
    }

    public List<T> getAll(Class<T> clazz) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = String.format("from %s", clazz.getName());
            return session.createQuery(queryString, clazz).list();
        }
    }
}

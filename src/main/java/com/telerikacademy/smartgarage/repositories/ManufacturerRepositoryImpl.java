package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Manufacturer;
import com.telerikacademy.smartgarage.models.ManufacturerSearchParameters;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ManufacturerRepositoryImpl extends AbstractGenericCrudRepository<Manufacturer> implements ManufacturerRepository {
    @Autowired
    public ManufacturerRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<Manufacturer> getAll() {
        return super.getAll(Manufacturer.class);
    }

    @Override
    public List<Manufacturer> getAll(ManufacturerSearchParameters parameters) {
        try (Session session = sessionFactory.openSession()) {
            String queryStr = "from Manufacturer ";
            var filters = new ArrayList<String>();
            if (parameters.getName() != null) {
                filters.add(" lower(name) like concat('%',:name,'%') ");
            }
            if (filters.size() > 0) {
                var finalQuery = String.join(" and ", filters);
                queryStr = queryStr + " where " + finalQuery;
            }
            Query<Manufacturer> query = session.createQuery(queryStr, Manufacturer.class);
            var name = parameters.getName();
            if (name != null) {
                query.setParameter("name", name);
            }
            return query.list();
        }
    }

    @Override
    public Manufacturer getById(Integer id) {
        return super.getByField("id", id, Manufacturer.class);
    }

    @Override
    public Manufacturer getByName(String name) {
        return super.getByField("name", name, Manufacturer.class);
    }

    @Override
    public Manufacturer create(Manufacturer manufacturer) {
        return super.create(manufacturer);
    }

    @Override
    public Manufacturer update(Manufacturer manufacturer) {
        return super.update(manufacturer);
    }
}

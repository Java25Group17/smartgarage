package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.VehicleCategory;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleCategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

 @Repository
public class VehicleCategoryRepositoryImpl extends AbstractGenericCrudRepository<VehicleCategory> implements VehicleCategoryRepository {
    @Autowired
    public VehicleCategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<VehicleCategory> getAll() {
        return super.getAll(VehicleCategory.class);
    }

    @Override
    public VehicleCategory getById(Integer id) {
        return super.getByField("id", id, VehicleCategory.class);
    }

     @Override
     public VehicleCategory getByName(String value) {
         return super.getByField("name", value, VehicleCategory.class);
     }

     @Override
    public VehicleCategory create(VehicleCategory objectToCreate) {
        return super.create(objectToCreate);
    }

    @Override
    public VehicleCategory update(VehicleCategory objectToUpdate) {
        return super.update(objectToUpdate);
    }
}
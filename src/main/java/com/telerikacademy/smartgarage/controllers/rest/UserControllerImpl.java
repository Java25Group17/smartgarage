package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.*;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.UserCreateDto;
import com.telerikacademy.smartgarage.models.UserSearchParameters;
import com.telerikacademy.smartgarage.models.UserUpdateDto;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import com.telerikacademy.smartgarage.services.mappers.ModelUserCreateMapper;
import com.telerikacademy.smartgarage.services.mappers.ModelUserUpdateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserControllerImpl {

    private final UserService userService;
    private final ModelUserCreateMapper userCreateMapper;
    private final ModelUserUpdateMapper userUpdateMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public UserControllerImpl(UserService userService,
                              ModelUserCreateMapper userCreateMapper,
                              ModelUserUpdateMapper userUpdateMapper,
                              AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userCreateMapper = userCreateMapper;
        this.userUpdateMapper = userUpdateMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            return userService.getAll(userAuthenticator);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalEntityException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<User> filterByMultiple(@RequestParam(required = false) String firstName,
                                       @RequestParam(required = false) String lastName,
                                       @RequestParam(required = false) String email,
                                       @RequestParam(required = false) String phone,
                                       @RequestParam(required = false) String model,
                                       @RequestParam(required = false) String mark,
                                       @RequestParam(required = false) String fromDate,
                                       @RequestParam(required = false) String toDate,
                                       @RequestParam(required = false) String orderBy,
                                       @RequestParam(required = false) String enabled,
                                       @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);

            LocalDate dateFrom = LocalDate.EPOCH;
            if (fromDate != null)
                dateFrom = LocalDate.parse(fromDate);

            LocalDate dateTo = LocalDate.now();
            if (toDate != null)
                dateTo = LocalDate.parse(toDate);

            if (dateTo.isBefore(dateFrom))
                throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "toDate is before fromDate!");

            return userService.filterByMultiple(new UserSearchParameters(firstName, lastName,
                    email, phone, model, mark, dateFrom, dateTo, orderBy, enabled), userAuthenticator);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalEntityException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable Integer id) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            return userService.getById(id, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public User create(@RequestHeader HttpHeaders headers, @Valid @RequestBody UserCreateDto userCreateDto) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            User user = userCreateMapper.fromDto(userCreateDto);
            return userService.create(user, userAuthenticator);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers, @PathVariable Integer id, @Valid @RequestBody UserUpdateDto userUpdateDto) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            User user = userUpdateMapper.fromDto(userUpdateDto, id);
            return userService.update(user, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id, @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            userService.delete(id, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/resetPassword")
    public String resetPassword(@Valid @RequestParam("email") String email) {
        try {
            return userService.resetPassword(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/check")
    public String resetPasswordCheckToken(@Valid @RequestParam("token") String token) {
        try {
            return userService.resetPasswordCheckToken(token);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/update/password")
    public String resetPasswordSetNewPassword(@RequestParam("email") String email,
                                              @RequestParam("password") String password,
                                              @RequestParam("passwordConfirm") String passwordConfirm) {
        try {
            return userService.resetPasswordSetNewPassword(email, password, passwordConfirm);
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}


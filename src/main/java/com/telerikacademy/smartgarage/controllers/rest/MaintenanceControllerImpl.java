package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.*;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.services.contracts.MaintenanceService;
import com.telerikacademy.smartgarage.services.mappers.ModelMaintenanceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/api/maintenances")
public class MaintenanceControllerImpl {
    private final MaintenanceService service;
    private final ModelMaintenanceMapper maintenanceMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public MaintenanceControllerImpl(MaintenanceService service,
                                     ModelMaintenanceMapper maintenanceMapper,
                                     AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.maintenanceMapper = maintenanceMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Maintenance> getAll(@RequestHeader HttpHeaders headers,
                                    @RequestParam(required = false) Integer statusId,
                                    @RequestParam(required = false) Integer customerId,
                                    @RequestParam(required = false) Integer vehicleId,
                                    @RequestParam(required = false) String fromDate,
                                    @RequestParam(required = false) String toDate,
                                    @RequestParam(required = false) String orderByDate) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            var startDate = fromDate == null ? null : LocalDate.parse(fromDate, DateTimeFormatter.ISO_DATE);
            var endDate = toDate == null ? null : LocalDate.parse(toDate, DateTimeFormatter.ISO_DATE);
            return service.getAll(user, new MaintenanceSearchParameters(statusId, customerId, vehicleId, startDate, endDate, orderByDate));
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Maintenance getById(@RequestHeader HttpHeaders headers, @PathVariable Integer id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return service.getById(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Maintenance create(@RequestHeader HttpHeaders headers, @Valid @RequestBody MaintenanceDto dto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Maintenance objToCreate = maintenanceMapper.fromDto(dto);
            return service.create(user, objToCreate);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/services/")
    public Maintenance addServices(@RequestHeader HttpHeaders headers,
                                   @PathVariable Integer id,
                                   @Valid @RequestBody List<MaintenanceServiceItemDto> serviceItems) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return service.addServices(id, maintenanceMapper.fromDtoList(serviceItems, id), user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalEntityException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Maintenance update(@RequestHeader HttpHeaders headers,
                              @PathVariable Integer id,
                              @Valid @RequestBody MaintenanceDto maintenanceDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Maintenance maintenance = maintenanceMapper.fromDto(maintenanceDto, id);
            return service.update(maintenance, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalEntityException e) {
            throw new ResponseStatusException(HttpStatus.PRECONDITION_REQUIRED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/services/{serviceId}")
    public Maintenance removeService(@RequestHeader HttpHeaders headers,
                                     @PathVariable Integer id,
                                     @PathVariable Integer serviceId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return service.removeService(id, serviceId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalEntityException e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, e.getMessage());
        }
    }

    @GetMapping("/pdf")
    public String generatePDF(@RequestHeader HttpHeaders headers,
                              @RequestParam(required = false) Integer statusId,
                              @RequestParam(required = false) Integer customerId,
                              @RequestParam(required = false) Integer vehicleId,
                              @RequestParam(required = false) String fromDate,
                              @RequestParam(required = false) String toDate,
                              @RequestParam(required = false) String currency,
                              @RequestParam(required = false) String orderByDate) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            var startDate = fromDate == null ? null : LocalDate.parse(fromDate, DateTimeFormatter.ISO_DATE);
            var endDate = toDate == null ? null : LocalDate.parse(toDate, DateTimeFormatter.ISO_DATE);
            return service.generatePDF(new MaintenanceSearchParameters(statusId, customerId, vehicleId, startDate, endDate, orderByDate),currency);
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}

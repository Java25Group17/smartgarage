package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.VehicleCategory;
import com.telerikacademy.smartgarage.models.VehicleCategoryDto;
import com.telerikacademy.smartgarage.services.contracts.VehicleCategoryService;
import com.telerikacademy.smartgarage.services.mappers.ModelVehicleCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/vehicles/categories")
public class VehicleCategoryControllerImpl {

    private final VehicleCategoryService service;
    private final ModelVehicleCategoryMapper modelCategoryMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VehicleCategoryControllerImpl(VehicleCategoryService service,
                                         ModelVehicleCategoryMapper modelCategoryMapper,
                                         AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelCategoryMapper = modelCategoryMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{id}")
    public VehicleCategory getById(@PathVariable Integer id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("")
    public List<VehicleCategory> getAll() {
        try {
            return service.getAll();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public VehicleCategory create(@RequestBody VehicleCategoryDto categoryDto,
                                  @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            VehicleCategory category = modelCategoryMapper.fromDto(categoryDto);
            return service.create(category, userAuthenticator);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public VehicleCategory update(@PathVariable Integer id,
                                  @RequestBody VehicleCategoryDto categoryDto,
                                  @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            VehicleCategory category = modelCategoryMapper.fromDto(categoryDto, id);
            return service.update(category, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
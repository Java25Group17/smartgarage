package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.EngineType;
import com.telerikacademy.smartgarage.services.contracts.EngineTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/vehicles/engines")
public class EngineTypeControllerImpl {

        private final EngineTypeService service;

        @Autowired
        public EngineTypeControllerImpl(EngineTypeService service) {
            this.service = service;
        }

        @GetMapping("/{id}")
        public EngineType getById(@PathVariable Integer id) {
            try {
                return service.getById(id);
            } catch (EntityNotFoundException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }

        @GetMapping("")
        public List<EngineType> getAll() {
            try {
                return service.getAll();
            } catch (EntityNotFoundException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }
}

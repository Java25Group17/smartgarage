package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import com.telerikacademy.smartgarage.services.mappers.ModelModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/models")
public class ModelControllerImpl {
    private final ModelService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelModelMapper modelMapper;

    @Autowired
    public ModelControllerImpl(ModelService service,
                               AuthenticationHelper authenticationHelper,
                               ModelModelMapper modelMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public Model getById(@PathVariable Integer id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("")
    public List<Model> getAll(@RequestParam(required = false) String name,
                              @RequestParam(required = false) Integer manufacturer_id) {
        try {
            return service.getAll(new ModelSearchParameters(name, manufacturer_id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Model create(@RequestHeader HttpHeaders headers,
                        @Valid @RequestBody ModelDto dto) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            Model objToCreate = modelMapper.fromDto(dto);
            return service.create(objToCreate, userAuthenticator);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Model update(@PathVariable Integer id,
                        @RequestBody ModelDto dto,
                        @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            Model modelObj = modelMapper.fromDto(dto, id);
            return service.update(modelObj, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}

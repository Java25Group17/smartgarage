package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.ServiceDto;
import com.telerikacademy.smartgarage.models.ServiceSearchParameters;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.smartgarage.services.mappers.ModelServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/services")
public class ServiceControllerImpl {
    private final ServiceService service;
    private final ModelServiceMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ServiceControllerImpl(ServiceService service,
                                 ModelServiceMapper modelMapper,
                                 AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{id}")
    public Service getById(@PathVariable Integer id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("")
    public List<Service> getAll(@RequestParam(required = false) String name,
                                @RequestParam(required = false) Double minPrice,
                                @RequestParam(required = false) Double maxPrice) {
        try {
            return service.getAll(new ServiceSearchParameters(name, minPrice, maxPrice, true));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/admin")
    public List<Service> getAllAdmin(@RequestHeader HttpHeaders headers, @RequestParam(required = false) String registrationNumber) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            return service.getAll(new ServiceSearchParameters());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Service create(@RequestBody ServiceDto dto,
                          @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            Service serviceObj = modelMapper.fromDto(dto);
            return service.create(serviceObj, userAuthenticator);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Service update(@PathVariable Integer id,
                          @RequestBody ServiceDto dto,
                          @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            Service serviceObj = modelMapper.fromDto(dto, id);
            return service.update(serviceObj, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id,
                       @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            service.delete(id, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}

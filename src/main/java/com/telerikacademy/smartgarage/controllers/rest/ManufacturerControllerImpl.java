package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.services.contracts.ManufacturerService;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import com.telerikacademy.smartgarage.services.mappers.ManufacturerModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("api/manufacturers")
public class ManufacturerControllerImpl {
    private final ManufacturerService service;
    private final ModelService modelService;
    private final AuthenticationHelper authenticationHelper;
    private final ManufacturerModelMapper modelMapper;

    @Autowired
    public ManufacturerControllerImpl(ManufacturerService service,
                                      ModelService modelService,
                                      AuthenticationHelper authenticationHelper,
                                      ManufacturerModelMapper modelMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.modelService = modelService;
    }

    @GetMapping("/{id}")
    public Manufacturer getById(@PathVariable Integer id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("")
    public List<Manufacturer> getAll(@RequestParam(required = false) String name) {
        try {
            return service.getAll(new ManufacturerSearchParameters(name));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Manufacturer create(@RequestBody ManufacturerDto dto,
                          @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            Manufacturer objToCreate = modelMapper.fromDto(dto);
            return service.create(objToCreate, userAuthenticator);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Manufacturer update(@PathVariable Integer id,
                          @RequestBody ManufacturerDto dto,
                          @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            Manufacturer manufacturerObj = modelMapper.fromDto(dto, id);
            return service.update(manufacturerObj, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}/models")
    public List<Model> getAllAjax(@PathVariable Integer id) {
        try {
            return  modelService.getAll(new ModelSearchParameters(null, id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

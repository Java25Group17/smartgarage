package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/currency")
public class CurrencyControllerImpl {
    private final CurrencyService currencyService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CurrencyControllerImpl(CurrencyService currencyService, AuthenticationHelper authenticationHelper) {
        this.currencyService = currencyService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Currency> getAll(@RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            return currencyService.getAll(userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Currency getById(@RequestHeader HttpHeaders headers, @PathVariable Integer id) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            return currencyService.getById(id, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}

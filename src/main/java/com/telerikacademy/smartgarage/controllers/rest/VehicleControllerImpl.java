package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import com.telerikacademy.smartgarage.services.mappers.ModelVehicleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleControllerImpl {

    private final VehicleService service;
    private final AuthenticationHelper authenticationHelper;
    private final ModelVehicleMapper modelMapper;

    @Autowired
    public VehicleControllerImpl(VehicleService service,
                                 AuthenticationHelper authenticationHelper,
                                 ModelVehicleMapper modelMapper) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public Vehicle getById(@PathVariable Integer id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("")
    public List<Vehicle> getAll(@RequestHeader HttpHeaders headers,
                                @RequestParam(required = false) Integer customerId,
                                @RequestParam(required = false) Integer modelId,
                                @RequestParam(required = false) Integer manufacturerId,
                                @RequestParam(required = false) String registrationNumber) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            return service.getAll(userAuthenticator, new VehicleSearchParameters(customerId, modelId, manufacturerId, registrationNumber,true));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/admin")
    public List<Vehicle> getAllAdmin(@RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            return service.getAll(userAuthenticator, new VehicleSearchParameters());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public Vehicle create(@RequestBody @Valid VehicleDto dto,
                          @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            Vehicle vehicleObj = modelMapper.fromDto(dto);
            return service.create(vehicleObj, userAuthenticator);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Vehicle update(@PathVariable Integer id,
                          @RequestBody VehicleDto dto,
                          @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            Vehicle vehicleObj = modelMapper.fromDto(dto, id);
            return service.update(vehicleObj, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id,
                       @RequestHeader HttpHeaders headers) {
        try {
            User userAuthenticator = authenticationHelper.tryGetUser(headers);
            service.delete(id, userAuthenticator);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}

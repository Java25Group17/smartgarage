package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import com.telerikacademy.smartgarage.services.AuthorisationHelper;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;

@Controller
public class BasicAuthorizationMvcController {

    protected final AuthenticationHelper authenticationHelper;

    public BasicAuthorizationMvcController(AuthenticationHelper authenticationHelper) {
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("currentUser")
    public User addCurrentUser(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session);
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Nullable
    protected User getEmployee(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        /* Allow access only if the user is admin or employee */
        if (AuthorisationHelper.verifyUserHasRoles(currentUser, UserRoles.EMPLOYEE)) {
            return currentUser;
        }
        return null;
    }

    protected User getCustomer(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        /* Allow access only if the user is customer */
        if (AuthorisationHelper.verifyUserHasRoles(currentUser, UserRoles.CUSTOMER)) {
            return currentUser;
        }
        return null;
    }

    protected User getAdmin(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        /* Allow access only if the user is admin */
        if (AuthorisationHelper.verifyUserHasRoles(currentUser, UserRoles.ADMIN)) {
            return currentUser;
        }
        return null;
    }

    protected User getUser(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        return currentUser;
    }

    protected User getAdminOrEmployee(HttpSession session) {
        User currentUser = authenticationHelper.tryGetUser(session);
        /* Allow access only if the user is admin */
        if (AuthorisationHelper.verifyUserHasRoles(currentUser, UserRoles.ADMIN, UserRoles.EMPLOYEE)) {
            return currentUser;
        }
        return null;
    }
}

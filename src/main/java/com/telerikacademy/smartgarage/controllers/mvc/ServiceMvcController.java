package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.*;
import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.ServiceDto;
import com.telerikacademy.smartgarage.models.ServiceSearchParameters;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import com.telerikacademy.smartgarage.services.AuthorisationHelper;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import com.telerikacademy.smartgarage.services.mappers.ModelServiceMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/services")
public class ServiceMvcController extends BasicAuthorizationMvcController {
    private final ServiceService service;
    private final ModelServiceMapper mapper;

    public ServiceMvcController(AuthenticationHelper authenticationHelper,
                                ServiceService service,
                                ModelServiceMapper mapper) {
        super(authenticationHelper);
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public String showAllServices(Model model, HttpSession session)
            throws UnauthorizedOperationException, AuthenticationFailureException {
        User currentUser = getUser(session);
        ServiceSearchParameters parameters = new ServiceSearchParameters();
        parameters.setEnabled(!AuthorisationHelper.verifyUserHasRoles(currentUser, UserRoles.ADMIN));

        List<Service> services = service.getAll(parameters);
        model.addAttribute("services", services);
        model.addAttribute("searchParams", parameters);
        return "services";
    }

    @PostMapping
    public String handleSearchServices(Model model,
                                       HttpSession session,
                                       @ModelAttribute("searchParams") ServiceSearchParameters parameters,
                                       BindingResult bindingResult)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException {
        User currentUser = getUser(session);

        if (bindingResult.hasErrors()) {
            return "services";
        }
        parameters.setEnabled(AuthorisationHelper.verifyUserHasRoles(currentUser, UserRoles.ADMIN));
        List<Service> services = service.getAll(parameters);
        model.addAttribute("services", services);
        model.addAttribute("searchParams", parameters);

        return "services";
    }

    @GetMapping("/create")
    public String createService(HttpSession session,
                                Model model)
            throws UnauthorizedOperationException,
            EntityNotFoundException, AuthenticationFailureException {
        getAdminOrEmployee(session);
        model.addAttribute("serviceDto", new ServiceDto());
        return "service-form";
    }

    @PostMapping("/create")
    public String handleCreateService(HttpSession session,
                                      @Valid @ModelAttribute("serviceDto") ServiceDto serviceDto,
                                      BindingResult bindingResult)
            throws UnauthorizedOperationException, EntityNotFoundException, DuplicateEntityException {
        User currentUser = getAdminOrEmployee(session);
        if (bindingResult.hasErrors()) {
            return "service-form";
        }
        Service serviceToCreate = mapper.fromDto(serviceDto);
        service.create(serviceToCreate, currentUser);
        return "redirect:/services";
    }


    @GetMapping("/{id}/update")
    public String updateService(@PathVariable int id,
                                HttpSession session,
                                Model model)
            throws UnauthorizedOperationException, EntityNotFoundException, AuthenticationFailureException {
        User currentUser = getAdminOrEmployee(session);
        Service serviceToUpdate = service.getById(id);

        ServiceDto serviceDto = mapper.toDto(serviceToUpdate);
        model.addAttribute("serviceId", id);
        model.addAttribute("serviceDto", serviceDto);
        model.addAttribute("service", serviceToUpdate);
        return "service-form";
    }

    @PostMapping("/{id}/update")
    public String handleEditService(Model model,
                                    HttpSession session,
                                    @PathVariable int id,
                                    @Valid @ModelAttribute("serviceDto") ServiceDto serviceDto,
                                    BindingResult bindingResult)
            throws UnauthorizedOperationException, EntityNotFoundException, AuthenticationFailureException {
        User currentUser = getAdminOrEmployee(session);
        if (bindingResult.hasErrors()) {
            return "service-form";
        }

        try {
            Service serviceToUpdate = mapper.fromDto(serviceDto, id);

            service.update(serviceToUpdate, currentUser);
        } catch (IllegalEntityException e) {
            bindingResult.rejectValue("price", "price", e.getMessage());
            return "service-form";
        }
        return "redirect:/services";
    }

    @GetMapping("/{id}/delete")
    public String handleDeleteService(Model model,
                                      HttpSession session,
                                      @PathVariable int id)
            throws UnauthorizedOperationException, EntityNotFoundException, AuthenticationFailureException {
        User currentUser = getAdminOrEmployee(session);
        service.delete(id, currentUser);
        return "redirect:/services";
    }
}

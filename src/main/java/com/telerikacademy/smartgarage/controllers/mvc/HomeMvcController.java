package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController extends BasicAuthorizationMvcController {
    private final UserService userService;

    @Autowired
    public HomeMvcController(UserService userService,
                             AuthenticationHelper authenticationHelper) {
        super(authenticationHelper);
        this.userService = userService;
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);

        } catch (UnauthorizedOperationException | AuthenticationFailureException | EntityNotFoundException e) {
            currentUser = null;
        }
        model.addAttribute("currentUser", currentUser);
        return "index";
    }
}

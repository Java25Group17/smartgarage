package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.*;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import com.telerikacademy.smartgarage.repositories.contracts.CurrencyRepository;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceServiceRepository;
import com.telerikacademy.smartgarage.services.AuthorisationHelper;
import com.telerikacademy.smartgarage.services.contracts.*;
import com.telerikacademy.smartgarage.services.mappers.ModelMaintenanceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.util.MimeTypeUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/maintenances")
public class MaintenanceMvcController extends BasicAuthorizationMvcController {
    private final MaintenanceService maintenanceService;
    private final ModelMaintenanceMapper maintenanceMapper;
    private final VehicleService vehicleService;
    private final ServiceService serviceService;
    private final StatusService statusService;
    private final CurrencyRepository currencyRepository;
    private final MaintenanceServiceRepository maintenanceServiceRepository;

    @Autowired
    public MaintenanceMvcController(AuthenticationHelper authenticationHelper,
                                    MaintenanceService maintenanceService,
                                    ModelMaintenanceMapper maintenanceMapper,
                                    VehicleService vehicleService,
                                    ServiceService serviceService,
                                    StatusService statusService,
                                    CurrencyRepository currencyRepository,
                                    MaintenanceServiceRepository maintenanceServiceRepository) {
        super(authenticationHelper);
        this.maintenanceService = maintenanceService;
        this.maintenanceMapper = maintenanceMapper;
        this.vehicleService = vehicleService;
        this.serviceService = serviceService;
        this.statusService = statusService;
        this.currencyRepository = currencyRepository;
        this.maintenanceServiceRepository = maintenanceServiceRepository;
    }

    @GetMapping
    public String showAllMaintenances(Model model, HttpSession session) throws UnauthorizedOperationException {
        User currentUser = getUser(session);
        MaintenanceSearchParameters parameters = new MaintenanceSearchParameters();
        if (AuthorisationHelper.verifyUserHasRoles(currentUser, UserRoles.CUSTOMER)) {
            parameters.setCustomerId(currentUser.getId());
        }
        List<Maintenance> maintenances = maintenanceService.getAll(currentUser, parameters);
        maintenances.stream().forEach(maintenance -> maintenance.getMaintenanceServices().toArray());
        model.addAttribute("maintenances", maintenances);
        model.addAttribute("maintenanceSearchDto", new MaintenanceSearchDto());
        return "maintenances";
    }

    @PostMapping
    public String handleSearchMaintenances(Model model,
                                           HttpSession session,
                                           @ModelAttribute("maintenanceSearchDto") MaintenanceSearchDto searchDto,
                                           BindingResult bindingResult)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException {
        User user = getUser(session);

        if (bindingResult.hasErrors()) {
            return "maintenances";
        }
        MaintenanceSearchParameters searchParameters = maintenanceMapper.fromDto(searchDto);
        if (AuthorisationHelper.verifyUserHasRoles(user, UserRoles.CUSTOMER)) {
            searchParameters.setCustomerId(user.getId());
        }
        List<Maintenance> maintenances = maintenanceService.getAll(
                user,
                searchParameters);
        model.addAttribute("maintenances", maintenances);
        model.addAttribute("maintenanceSearchDto", searchDto);

        return "maintenances";
    }

    @PostMapping("/email")
    public String emailInvoice(Model model,
                               HttpSession session,
                               @ModelAttribute("maintenanceSearchDto") MaintenanceSearchDto searchDto,
                               BindingResult bindingResult)
            throws IllegalEntityException {
        User user = getUser(session);
        MaintenanceSearchParameters searchParameters = maintenanceMapper.fromDto(searchDto);
        if (AuthorisationHelper.verifyUserHasRoles(user, UserRoles.CUSTOMER)) {
            searchParameters.setCustomerId(user.getId());
        }
        maintenanceService.sendPDFByMail(searchParameters, searchDto.getCurrencyAbbr());
        return "maintenances";
    }

    @PostMapping("/generate")
    public void downloadFile(HttpSession session,
                             @ModelAttribute("maintenanceSearchDto") MaintenanceSearchDto searchDto,
                             HttpServletResponse resp) throws IOException {

        User user = getUser(session);

        MaintenanceSearchParameters searchParameters = maintenanceMapper.fromDto(searchDto);
        if (AuthorisationHelper.verifyUserHasRoles(user, UserRoles.CUSTOMER)) {
            searchParameters.setCustomerId(user.getId());
        }
        maintenanceService.generatePDF(searchParameters, searchDto.getCurrencyAbbr());
        Path fileLocation = Paths.get("src/main/resources/pdf/pdfInvoice.pdf");

        byte[] byteArray = Files.readAllBytes(fileLocation); // read the byteArray

        resp.setContentType(MimeTypeUtils.APPLICATION_OCTET_STREAM.getType());
        resp.setHeader("Content-Disposition", "PDF");
        resp.setContentLength(byteArray.length);

        OutputStream os = resp.getOutputStream();
        try {
            os.write(byteArray, 0, byteArray.length);
        } finally {
            os.close();
        }
    }

    @GetMapping("/create/{vehicleId}")
    public String createMaintenance(@PathVariable int vehicleId,
                                    HttpSession session,
                                    Model model)
            throws UnauthorizedOperationException,
            EntityNotFoundException {
        User currentUser = getAdminOrEmployee(session);
        MaintenanceDto dto = new MaintenanceDto();
        dto.setVehicleId(vehicleId);
        maintenanceService.create(currentUser, maintenanceMapper.fromDto(dto));
        return "redirect:/maintenances";
    }

    @GetMapping("/{id}/update")
    public String updateMaintenance(@PathVariable int id,
                                    HttpSession session,
                                    Model model)
            throws UnauthorizedOperationException, EntityNotFoundException {
        User currentUser = getAdminOrEmployee(session);
        Maintenance maintenanceToUpdate = maintenanceService.getById(id, currentUser);

        MaintenanceDto maintenanceDto = maintenanceMapper.toDto(maintenanceToUpdate);
        model.addAttribute("maintenanceId", id);
        model.addAttribute("maintenanceDto", maintenanceDto);
        model.addAttribute("maintenance", maintenanceToUpdate);
        return "maintenance-form";
    }

    @PostMapping("/{id}/update")
    public String handleEditMaintenance(Model model,
                                        HttpSession session,
                                        @PathVariable int id,
                                        @Valid @ModelAttribute("maintenanceDto") MaintenanceDto maintenanceDto,
                                        BindingResult bindingResult)
            throws UnauthorizedOperationException, EntityNotFoundException {
        User currentUser = getAdminOrEmployee(session);
        if (bindingResult.hasErrors()) {
            return "maintenance-form";
        }

        try {
            Maintenance maintenance = maintenanceMapper.fromDto(maintenanceDto, id);
            maintenanceService.update(maintenance, currentUser);
        } catch (IllegalEntityException e) {
            bindingResult.rejectValue("toDate", "to date", e.getMessage());
            return "maintenance-form";
        }
        return "redirect:/maintenances";
    }

    @GetMapping("{id}/serviceItem")
    public String addServiceItem(@PathVariable int id,
                                 HttpSession session,
                                 Model model)
            throws UnauthorizedOperationException, EntityNotFoundException {
        User currentUser = getAdminOrEmployee(session);
        MaintenanceServiceItemDto dto = new MaintenanceServiceItemDto();
        dto.setMaintenanceId(id);
        dto.setHours(0);
        List<Service> services = getServices();
        Maintenance maintenance = maintenanceService.getById(id, currentUser);
        var addedServices = maintenance.getMaintenanceServices()
                .stream().map(serviceItem -> serviceItem.getService()).collect(Collectors.toList());
        services.removeAll(addedServices);
        model.addAttribute("serviceItemId", null);
        model.addAttribute("services", services);
        model.addAttribute("serviceItemDto", dto);
        return "maintenance-service";
    }

    private List<Service> getServices() {
        List<Service> services = serviceService.
                getAll(new ServiceSearchParameters(null, null, null, true));
        return services;
    }

    @PostMapping("{id}/serviceItem")
    public String handleAddServiceItem(HttpSession session,
                                       @PathVariable int id,
                                       @Valid @ModelAttribute("serviceItemDto") MaintenanceServiceItemDto dto,
                                       BindingResult bindingResult)
            throws UnauthorizedOperationException, EntityNotFoundException {
        User currentUser = getAdminOrEmployee(session);
        if (bindingResult.hasErrors()) {
            return "maintenance-service";
        }
        Maintenance maintenance = maintenanceService.getById(id, currentUser);
        MaintenanceServiceItem item = maintenanceMapper.fromDto(dto);
        var serviceItems = maintenance.getMaintenanceServices();
        serviceItems.add(item);
        maintenance.setMaintenanceServices(serviceItems);
        maintenanceService.update(maintenance, currentUser);
        return "redirect:/maintenances/" + id + "/update";
    }

    @GetMapping("{id}/serviceItem/{serviceItemId}")
    public String updateServiceItem(@PathVariable Integer id,
                                    @PathVariable Integer serviceItemId,
                                    HttpSession session,
                                    Model model)
            throws UnauthorizedOperationException, EntityNotFoundException {
        User currentUser = getAdminOrEmployee(session);
        MaintenanceServiceItem item = maintenanceServiceRepository.getById(serviceItemId);
        MaintenanceServiceItemDto dto = maintenanceMapper.toDto(item);
        model.addAttribute("serviceItemId", serviceItemId);
        model.addAttribute("services", getServices());
        model.addAttribute("serviceItemDto", dto);
        return "maintenance-service";
    }

    @PostMapping("{id}/serviceItem/{serviceItemId}")
    public String handleUpdateServiceItem(HttpSession session,
                                          @PathVariable Integer id,
                                          @PathVariable Integer serviceItemId,
                                          @Valid @ModelAttribute("serviceItemDto") MaintenanceServiceItemDto dto,
                                          BindingResult bindingResult)
            throws UnauthorizedOperationException, EntityNotFoundException {
        User currentUser = getAdminOrEmployee(session);

        Maintenance maintenance = maintenanceService.getById(id, currentUser);
        MaintenanceServiceItem item = maintenanceMapper.fromDto(dto, serviceItemId);
        var serviceItems = maintenance.getMaintenanceServices();
        serviceItems = serviceItems.stream().filter(serviceItem -> serviceItem.getId() != serviceItemId).collect(Collectors.toList());
        serviceItems.add(item);
        maintenance.setMaintenanceServices(serviceItems);
        maintenanceService.update(maintenance, currentUser);
        return "redirect:/maintenances/" + id + "/update";
    }

    @GetMapping("/{id}/delete/{serviceItemId}")
    public String handleDeleteServiceItem(Model model,
                                          HttpSession session,
                                          @PathVariable int id,
                                          @PathVariable int serviceItemId)
            throws UnauthorizedOperationException, EntityNotFoundException, AuthenticationFailureException {
        User currentUser = getAdminOrEmployee(session);
        maintenanceService.removeService(id, serviceItemId, currentUser);
        return "redirect:/maintenances/" + id + "/update";
    }

    @ModelAttribute("statuses")
    public List<MaintenanceStatus> populateStatuses() {
        return statusService.getAll();
    }

    @ModelAttribute("currencies")
    public List<Currency> populateCurrencies() {
        return currencyRepository.getAll();
    }

    @ModelAttribute("vehicles")
    public List<Vehicle> populateVehicles(HttpSession session) {
        User user = getUser(session);
        VehicleSearchParameters parameters = new VehicleSearchParameters();
        if (AuthorisationHelper.verifyUserHasRoles(user, UserRoles.CUSTOMER)) {
            parameters.setCustomerId(user.getId());
        }
        if (AuthorisationHelper.verifyUserHasRoles(user, UserRoles.ADMIN)) {
            parameters.setEnabled(false);
        } else {
            parameters.setEnabled(true);
        }
        return vehicleService.getAll(user, parameters);
    }
}

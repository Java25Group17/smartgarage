package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.utils.MailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/cfmessage")
public class MessageMvcController {
    private final String username = "smart.garage.g17@gmail.com";
    private final String SUBJECT_NEW_MESSAGE = "Contact form";

    public MessageMvcController() {
    }

    @PostMapping("/mess")
    public String sendMail(@RequestParam(required = false) String name,
                           @RequestParam(required = false) String email,
                           @RequestParam(required = false) String phone,
                           @RequestParam(required = false) String message) {
        try {
            StringBuilder mail = new StringBuilder();
            mail.append("Name: ").append(name);
            mail.append(System.lineSeparator());
            mail.append("Email: ").append(email);
            mail.append(System.lineSeparator());
            mail.append("Phone: ").append(phone);
            mail.append(System.lineSeparator());
            mail.append("Message: ").append(message);
            MailSender ms = new MailSender();
            if (!ms.sendMail(SUBJECT_NEW_MESSAGE, mail.toString())) {
                throw new AuthenticationFailureException(ms.getErrorTrace());
            }
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "/";
        }
    }
}

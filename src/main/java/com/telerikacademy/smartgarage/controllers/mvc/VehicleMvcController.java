package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.*;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.repositories.contracts.EngineTypeRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleCategoryRepository;
import com.telerikacademy.smartgarage.services.contracts.ManufacturerService;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import com.telerikacademy.smartgarage.services.mappers.ModelVehicleMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/vehicles")
public class VehicleMvcController extends BasicAuthorizationMvcController {
    private final VehicleService vehicleService;
    private final ManufacturerService manufacturerService;
    private final ModelVehicleMapper modelVehicleMapper;
    private final UserRepository userRepository;
    private final VehicleCategoryRepository vehicleCategoryRepository;
    private final EngineTypeRepository engineTypeRepository;
    private final ModelRepository modelRepository;

    public VehicleMvcController(AuthenticationHelper authenticationHelper,
                                VehicleService vehicleService,
                                ManufacturerService manufacturerService,
                                ModelVehicleMapper modelVehicleMapper,
                                UserRepository userRepository,
                                VehicleCategoryRepository vehicleCategoryRepository,
                                EngineTypeRepository engineTypeRepository,
                                ModelRepository modelRepository) {
        super(authenticationHelper);
        this.vehicleService = vehicleService;
        this.manufacturerService = manufacturerService;
        this.modelVehicleMapper = modelVehicleMapper;
        this.userRepository = userRepository;
        this.vehicleCategoryRepository = vehicleCategoryRepository;
        this.engineTypeRepository = engineTypeRepository;
        this.modelRepository = modelRepository;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    //Show all vehicles by search criteria
    @GetMapping
    public String showByMultiple(HttpSession session, Model model)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException {

        User currentUser;
        List<Vehicle> vehicles;
        try {
            currentUser = getAdminOrEmployee(session);
            VehicleSearchParameters vehicleSearchParameters = new VehicleSearchParameters();
            vehicleSearchParameters.setEnabled(true);
            vehicles = vehicleService.getAll(currentUser, vehicleSearchParameters);

        } catch (UnauthorizedOperationException e) {
            return "error";
        }
        model.addAttribute("vehicles", vehicles);
        model.addAttribute("vehicleSearchParameters", new VehicleSearchParameters());
        model.addAttribute("users", userRepository.getAll());
        model.addAttribute("carlist", manufacturerService.getAll(new ManufacturerSearchParameters()));
        return "vehicles-form";
    }

    //Send search criteria
    @PostMapping("/search")
    public String handleSearchVehicle(Model model,
                                      HttpSession session,
                                      @ModelAttribute("vehicleSearchParameters") VehicleSearchParameters vehicleSearchParametersAjax,
                                      BindingResult bindingResult)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException,
            DuplicateEntityException {
        User currentUser;
        try {
            currentUser = getAdminOrEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "error";
        }
        List<Vehicle> vehicles;
        try {
            VehicleSearchParameters vehicleSearchParameters = new VehicleSearchParameters();

            if (vehicleSearchParametersAjax.getCustomerId() == null
                    || vehicleSearchParametersAjax.getCustomerId() == -1)
                vehicleSearchParameters.setCustomerId(null);
            else
                vehicleSearchParameters.setCustomerId(vehicleSearchParametersAjax.getCustomerId());

            if (vehicleSearchParametersAjax.getModelId() == null
                    || vehicleSearchParametersAjax.getModelId() == -1)
                vehicleSearchParameters.setModelId(null);
            else
                vehicleSearchParameters.setModelId(vehicleSearchParametersAjax.getModelId());

            if (vehicleSearchParametersAjax.getManufacturerId() == null
                    || vehicleSearchParametersAjax.getManufacturerId() == -1
                    || vehicleSearchParametersAjax.getManufacturerId() == 0)
                vehicleSearchParameters.setManufacturerId(null);
            else
                vehicleSearchParameters.setManufacturerId(vehicleSearchParametersAjax.getManufacturerId());

            if (vehicleSearchParametersAjax.getRegistrationNumber() == null
                    || vehicleSearchParametersAjax.getRegistrationNumber().isBlank())
                vehicleSearchParameters.setRegistrationNumber(null);
            else
                vehicleSearchParameters.setRegistrationNumber(vehicleSearchParametersAjax.getRegistrationNumber());

            vehicleSearchParameters.setEnabled(true);

            vehicles = vehicleService.getAll(currentUser, vehicleSearchParameters);

            model.addAttribute("vehicles", vehicles);

            return "fragments/vehicle.html :: vehicle-table-data";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("Vehicle", "id", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}")
    public String showVehicle(@PathVariable int id, HttpSession session, Model model)
            throws AuthenticationFailureException,
            EntityNotFoundException {
        try {
            Vehicle vehicle = vehicleService.getById(id);
            model.addAttribute("vehicle", vehicle);
            return "fragments/vehicle.html :: vehicle-table-data";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/create")
    public String showNewVehiclePage(Model model, HttpSession session)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            DuplicateEntityException {
        User currentUser;
        try {
            currentUser = getAdminOrEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "error";
        }
        model.addAttribute("vehicle", new VehicleCreateDto());
        model.addAttribute("users", userRepository.getAll());
        model.addAttribute("engines", engineTypeRepository.getAll());
        model.addAttribute("categories", vehicleCategoryRepository.getAll());
        model.addAttribute("carlist", manufacturerService.getAll(new ManufacturerSearchParameters()));
        return "new-vehicle-form";
    }

    @GetMapping("/create/{id}")
    public String showNewVehiclePageOneUser(@PathVariable int id, Model model, HttpSession session)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            DuplicateEntityException {
        User currentUser;
        try {
            currentUser = getAdminOrEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "error";
        }
        model.addAttribute("vehicle", new VehicleCreateDto());
        model.addAttribute("users", userRepository.getById(id));
        model.addAttribute("oneuser", id);
        model.addAttribute("engines", engineTypeRepository.getAll());
        model.addAttribute("categories", vehicleCategoryRepository.getAll());
        model.addAttribute("carlist", manufacturerService.getAll(new ManufacturerSearchParameters()));
        return "new-vehicle-form";
    }

    @PostMapping("/create")
    public String createVehicle(HttpSession session, Model model,
                                @Valid @ModelAttribute("vehicle") VehicleCreateDto vehicleCreateDto,
                                BindingResult errors)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            DuplicateEntityException {
        User currentUser;
        try {
            currentUser = getAdminOrEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "error";
        }

        if (errors.hasErrors()) {
            return "error";
        }

        Vehicle newVehicle = modelVehicleMapper.fromCreateDto(vehicleCreateDto);

        int carMakerId;
        int carModelId;
        if (vehicleCreateDto.getManufacturerId() == null
                || Integer.parseInt(vehicleCreateDto.getManufacturerId().toString()) < 1) {
            Manufacturer newManufacturer = new Manufacturer();
            if (vehicleCreateDto.getManufacturerName().isBlank()) {
                throw new IllegalEntityException("Please choose vehicle or enter new one!");
            } else {
                newManufacturer.setName(vehicleCreateDto.getManufacturerName());
                carMakerId = manufacturerService.create(newManufacturer,currentUser).getId();
            }
        } else {
            carMakerId = vehicleCreateDto.getManufacturerId();
        }

        if (vehicleCreateDto.getModelId() == null
                || vehicleCreateDto.getModelId() == -1) {
            if (vehicleCreateDto.getModelName().isBlank()) {
                throw new IllegalEntityException("Please choose model or enter new one!");
            } else {
                com.telerikacademy.smartgarage.models.Model vehicleModel = new com.telerikacademy.smartgarage.models.Model();
                vehicleModel.setManufacturer(manufacturerService.getById(carMakerId));
                vehicleModel.setName(vehicleCreateDto.getModelName());
                carModelId = modelRepository.create(vehicleModel).getId();
            }
        } else {
            carModelId = vehicleCreateDto.getModelId();
        }

        newVehicle.setModel(modelRepository.getById(carModelId));

        try {
            vehicleService.create(newVehicle, currentUser);
            return "redirect:/vehicles-form";
        }
        catch (DuplicateEntityException e) {
            errors.rejectValue("identificationNumber", "vehicle.exist", e.getMessage());
            model.addAttribute("errorMessage", errors);
            return "nok";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditVehiclePage(@PathVariable int id,
                                      Model model,
                                      HttpSession session)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException {
        User user;
        try {
            user = getAdminOrEmployee(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Vehicle vehicleToUpdate = vehicleService.getById(id);
            VehicleDto vehicleDto = modelVehicleMapper.toDto(vehicleToUpdate);
            model.addAttribute("vehicleId", id);
            model.addAttribute("vehicle", vehicleDto);
            return "new-vehicle-form";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/update")
    public String updateVehicle(@PathVariable int id,
                                @Valid @ModelAttribute("vehicle") VehicleDto dto,
                                BindingResult errors,
                                Model model,
                                HttpSession session)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException,
            DuplicateEntityException {
        User user;
        try {
            user = getAdminOrEmployee(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "error";
        }

        try {
            Vehicle vehicleToUpdate = modelVehicleMapper.fromDto(dto, id);
            vehicleService.update(vehicleToUpdate, user);
            return "redirect:/vehicles";

        } catch (DuplicateEntityException e) {
            errors.rejectValue("vin", "duplicate_vehicle", e.getMessage());
            return "profile-vehicle";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteVehicle(@PathVariable int id, Model model, HttpSession session)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException {
        User user;
        try {
            user = getAdminOrEmployee(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            vehicleService.delete(id, user);
            return "redirect:/vehicles";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}

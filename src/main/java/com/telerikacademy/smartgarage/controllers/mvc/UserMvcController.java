package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.*;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.services.contracts.ManufacturerService;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import com.telerikacademy.smartgarage.services.mappers.ModelUserCreateMapper;
import com.telerikacademy.smartgarage.services.mappers.ModelUserUpdateMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController extends BasicAuthorizationMvcController {
    private final UserService userService;
    private final ManufacturerService manufacturerService;
    private final ModelUserCreateMapper userCreateMapper;
    private final ModelUserUpdateMapper userUpdateMapper;

    public UserMvcController(UserService userService,
                             AuthenticationHelper authenticationHelper,
                             ManufacturerService manufacturerService,
                             ModelUserCreateMapper userCreateMapper,
                             ModelUserUpdateMapper userUpdateMapper) {
        super(authenticationHelper);
        this.userService = userService;
        this.manufacturerService = manufacturerService;
        this.userUpdateMapper = userUpdateMapper;
        this.userCreateMapper = userCreateMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    //Create new customer and return all
    @PostMapping("/")
    public String handleAllUsers(Model model,
                                 HttpSession session,
                                 @ModelAttribute("users") User user,
                                 BindingResult bindingResult)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException,
            DuplicateEntityException {
        User currentUser;
        try {
            currentUser = getAdminOrEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "register";
        }

        try {
            List<User> users = userService.getAll(currentUser);

            model.addAttribute("users", users);
            return "users-form";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("User", "id", e.getMessage());
            return "register";
        }
    }

    //Show all customers by search criteria
    @GetMapping
    public String showByMultiple(HttpSession session, Model model)
//            throws AuthenticationFailureException,
//            UnauthorizedOperationException,
//            EntityNotFoundException {
            throws UnauthorizedOperationException,
            EntityNotFoundException {

        User currentUser;
        List<User> users;
        try {
            currentUser = getAdminOrEmployee(session);
            users = userService.getAll(currentUser);

        } catch (UnauthorizedOperationException e) {
            return "error";
        }
        model.addAttribute("users", users);
        model.addAttribute("userSearchParameters", new UserSearchParametersAjax());
        model.addAttribute("carlist",
                manufacturerService.getAll(new ManufacturerSearchParameters()));
        return "users-form";
    }

    //Send search criteria
    @PostMapping("/search")
    public String handleSearchUser(Model model,
                                   HttpSession session,
                                   @ModelAttribute("userSearchParameters") UserSearchParametersAjax userSearchParametersAjax,
                                   BindingResult bindingResult)
            throws UnauthorizedOperationException,
            IllegalEntityException,
            EntityNotFoundException {
        User currentUser;
        try {
            currentUser = getAdminOrEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "register";
        }
        List<User> users;
        try {
            UserSearchParameters userSearchParameters = new UserSearchParameters();

            userSearchParameters.setFirstName(userSearchParametersAjax.getFirstName());
            userSearchParameters.setLastName(userSearchParametersAjax.getLastName());
            userSearchParameters.setEmail(userSearchParametersAjax.getEmail());
            userSearchParameters.setPhone(userSearchParametersAjax.getPhone());

            if (userSearchParametersAjax.getMark() == null
                    || userSearchParametersAjax.getMark().equals("-1")
                    || userSearchParametersAjax.getMark().equals("0"))
                userSearchParameters.setMark("");
            else
                userSearchParameters.setMark(userSearchParametersAjax.getMark());

            if (userSearchParametersAjax.getModel() == null || userSearchParametersAjax.getModel().equals("-1"))
                userSearchParameters.setModel("");
            else
                userSearchParameters.setModel(userSearchParametersAjax.getModel());

            if (userSearchParametersAjax.getFromDate() != null && !userSearchParametersAjax.getFromDate().isBlank())
                userSearchParameters.setFromDate(LocalDate.parse(userSearchParametersAjax.getFromDate()));
            else
                userSearchParameters.setFromDate(LocalDate.EPOCH);

            if (userSearchParametersAjax.getToDate() != null && !userSearchParametersAjax.getToDate().isBlank())
                userSearchParameters.setToDate(LocalDate.parse(userSearchParametersAjax.getToDate()));
            else
                userSearchParameters.setToDate(LocalDate.now());

            if (userSearchParameters.getToDate().isBefore(userSearchParameters.getFromDate()))
                throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "toDate is before fromDate!");

            if (userSearchParametersAjax.getOrderBy() == null || userSearchParametersAjax.getOrderBy().equals("-1"))
                userSearchParameters.setOrderBy("");
            else
                userSearchParameters.setOrderBy(userSearchParametersAjax.getOrderBy());

            users = userService.filterByMultiple(userSearchParameters, currentUser);

            model.addAttribute("users", users);

            return "fragments/user.html :: user-table-data";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("User", "id", e.getMessage());
            return "register";
        }
    }

    @GetMapping("/{id}")
    public String showUser(@PathVariable int id, HttpSession session, Model model)
            throws AuthenticationFailureException,
            EntityNotFoundException {
        try {
            User user = userService.getById(id, getCustomer(session));
            model.addAttribute("user", user);
            return "register";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/customer")
    public String showProfile(HttpSession session,
                              Model model)
            throws UnauthorizedOperationException,
            AuthenticationFailureException,
            EntityNotFoundException {
        User currentUser;
        try {
            currentUser = getUser(session);
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            return "error";
        }

        try {
            UserUpdateDto userDto = userUpdateMapper.toDto(currentUser);
            model.addAttribute("user", userDto);

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "profile-customer";
    }

    @GetMapping("/create")
    public String showNewCustomerPage(Model model, HttpSession session)
            throws UnauthorizedOperationException {
        User currentUser;
        try {
            currentUser = getAdminOrEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "error";
        }
        model.addAttribute("user", new UserCreateDto());
        return "register";
    }

    @PostMapping("/create")
    public String createCustomer(HttpSession session,
                                 @Valid @ModelAttribute("user") UserCreateDto user,
                                 BindingResult errors)
            throws UnauthorizedOperationException {
        User currentUser;
        try {
            currentUser = getAdminOrEmployee(session);
        } catch (UnauthorizedOperationException e) {
            return "error";
        }

        if (errors.hasErrors()) {
            return "register";
        }
        try {
            User newUser = userCreateMapper.fromDto(user);
            userService.create(newUser, currentUser);
            return "redirect:/users";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "user.exist", e.getMessage());
            return "register";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id,
                                   Model model,
                                   HttpSession session)
            throws UnauthorizedOperationException,
            EntityNotFoundException {
        User user;
        try {
            user = getAdminOrEmployee(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            User userToUpdate = userService.getById(id, user);
            UserUpdateDto userDto = userUpdateMapper.toDto(userToUpdate);
            model.addAttribute("userId", id);
            model.addAttribute("user", userDto);
            return "profile-user";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UserUpdateDto dto,
                             BindingResult errors,
                             Model model,
                             HttpSession session)
            throws UnauthorizedOperationException,
                   EntityNotFoundException {
        User user;
        try {
            user = getAdminOrEmployee(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "error";
        }

        try {
            User userToUpdate = userUpdateMapper.fromDto(dto, id);
            userService.update(userToUpdate, user);
            return "redirect:/users";

        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "duplicate_user", e.getMessage());
            return "profile-user";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session)
            throws UnauthorizedOperationException,
            EntityNotFoundException {
        User user;
        try {
            user = getAdmin(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.delete(id, user);
            return "redirect:/users";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/password/reset")
    public String resetPassword(@ModelAttribute("login") LoginDto login,
                                BindingResult errors)
            throws EntityNotFoundException {
        if (errors.hasErrors()) {
            return "login";
        }
        try {
            userService.resetPassword(login.getEmail());
            return "redirect:/login";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("email", "user.not exist", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/check/{token}")
    public String resetPasswordCheckToken(@PathVariable String token, Model model)
            throws EntityNotFoundException {
        model.addAttribute("changePassword", new UpdatePasswordDto());
        try {
            userService.resetPasswordCheckToken(token);
            return "change-password";
        } catch (EntityNotFoundException e) {
            return "change-password";
        }
    }

    @GetMapping("/password/update")
    public String showChangePasswordForm(HttpSession session, Model model)
            throws AuthenticationFailureException,
            UnauthorizedOperationException,
            EntityNotFoundException {
        User currentUser;
        try {
            currentUser = getUser(session);
        } catch (UnauthorizedOperationException | AuthenticationFailureException e) {
            return "error";
        }

        try {
            UpdatePasswordDto updatePassword = new UpdatePasswordDto();
            updatePassword.setEmail(currentUser.getEmail());
            model.addAttribute("changePassword", updatePassword);

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
        return "change-password";
    }

    @PostMapping("/password/update")
    public String resetPasswordSetNewPassword(@Valid @ModelAttribute("changePassword") UpdatePasswordDto updatePassword,
                                              Model model,
                                              HttpSession session,
                                              BindingResult bindingResult)
            throws EntityNotFoundException {
        if (!updatePassword.getPassword().equals(updatePassword.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "password_error",
                    "Password confirmation should match password.");
            return "change-password";
        }
        if (bindingResult.hasErrors()) {
            return "change-password";
        }
        try {
            // User userToUpdate = userUpdateMapper.fromDto(updatePassword, getUser(session).getId());
            userService.resetPasswordSetNewPassword(
                    updatePassword.getEmail(),
                    updatePassword.getPassword(),
                    updatePassword.getConfirmPassword());
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return "change-password";
        }
    }
}

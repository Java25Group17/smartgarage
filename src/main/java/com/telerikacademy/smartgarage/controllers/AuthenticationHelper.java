package com.telerikacademy.smartgarage.controllers;

import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.utils.Sha256;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

@Component
public class AuthenticationHelper {
    private static final String REQUIRES_AUTHENTICATION = "The requested resource requires authentication.";
    private static final String INVALID_USERNAME = "Invalid username.";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";

    private final UserService userService;
    private final UserRepository userRepository;

    public AuthenticationHelper(UserService userService,
                                UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(HttpHeaders.AUTHORIZATION)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, REQUIRES_AUTHENTICATION);
        }
        try {
            List<String> list = headers.get(HttpHeaders.AUTHORIZATION);
            String[] tokens = list.get(0).split(" ");
            String authentication = tokens[0];
            if (!authentication.equals("Basic")) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Basic Authentication expected");
            }
            String[] decodedTokens = new String(Base64.getDecoder().decode(tokens[1])).split(":");
            String userEmail = decodedTokens[0];
            String userPassword = decodedTokens[1];
            return verifyAuthentication(userEmail, userPassword);

        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_USERNAME);
        }
    }

    public User tryGetUser(HttpSession session) {
        Integer currentUserId = (Integer) session.getAttribute("currentUserId");
        if (currentUserId == null) {
            throw new AuthenticationFailureException("No user logged in.");
        }
        User currentUser = userRepository.getById(currentUserId);
        if (currentUser == null) {
            throw new AuthenticationFailureException("No user logged in.");
        }
        return currentUser;
    }

    public User verifyAuthentication(String email, String password) {
        try {
            User user = userService.getByEmail(email);
            if (!user.getPassword().equals(generatePasswordHash(password))) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }

    public User verifyAuthorization(HttpSession session) {
        User user = tryGetUser(session);
        return user;
    }

    public static String generatePasswordHash(String password) {
        StringBuilder result = new StringBuilder();
        Sha256 hashPassword = new Sha256();
        try {
            result.append(hashPassword.getHash(password));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return result.toString();
    }
}
package com.telerikacademy.smartgarage.exceptions;

public class IllegalEntityException extends RuntimeException {
    public IllegalEntityException(String message) {
        super(message);
    }
}

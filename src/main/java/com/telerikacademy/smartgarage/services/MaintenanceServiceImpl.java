package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.repositories.contracts.*;
import com.telerikacademy.smartgarage.services.contracts.MaintenanceService;
import com.telerikacademy.smartgarage.utils.MailSender;
import com.telerikacademy.smartgarage.utils.PDFiText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyNotCustomer;

@Service
public class MaintenanceServiceImpl implements MaintenanceService {
    private static final String MAINTENANCE_FINISHED = "Maintenance finished!";
    private static final String SERVICE_NOT_IN_MAINTENANCE = "Service not in maintenance";
    public static final String DEFAULT_CURRENCY = "BGN";
    private static final String SUBJECT_INVOICE = "Invoice";
    private static final String INCORRECT_VALUE_ORDER = "Value for order should be asc or desc.";

    private final MaintenanceRepository repository;
    private final MaintenanceServiceRepository maintenanceServiceRepository;
    private final CurrencyRepository currencyRepository;
    private final UserRepository userRepository;
    private final VehicleRepository vehicleRepository;

    @Autowired
    public MaintenanceServiceImpl(MaintenanceRepository repository,
                                  MaintenanceServiceRepository maintenanceServiceRepository,
                                  CurrencyRepository currencyRepository,
                                  UserRepository userRepository, VehicleRepository vehicleRepository) {
        this.repository = repository;
        this.maintenanceServiceRepository = maintenanceServiceRepository;
        this.currencyRepository = currencyRepository;
        this.userRepository = userRepository;
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Maintenance> getAll(User user, MaintenanceSearchParameters parameters) {
//        verifyNotCustomer(user);
        if (parameters.getOrderByDate() != null &&
                !parameters.getOrderByDate().equalsIgnoreCase("asc") &&
                !parameters.getOrderByDate().equalsIgnoreCase("desc")) {
            throw new IllegalEntityException(INCORRECT_VALUE_ORDER);
        }
        return repository.getAll(parameters);
    }

    @Override
    public Maintenance getById(Integer id, User user) {
        verifyNotCustomer(user);
        return repository.getById(id);
    }

    @Override
    public Maintenance create(User user, Maintenance objectToCreate) {
        verifyNotCustomer(user);
        boolean duplicateExists = true;
        try {
            repository.getByNotFinishedVehicle(objectToCreate.getVehicle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Maintenance", "vehicle", objectToCreate.getVehicle().getRegistrationPlate());
        }
        return repository.create(objectToCreate);
    }

    @Override
    public Maintenance update(Maintenance objectToUpdate, User user) {
        verifyNotCustomer(user);
        repository.getByNotFinishedVehicle(objectToUpdate.getVehicle());
        return repository.update(objectToUpdate);
    }

    @Override
    public Maintenance addServices(Integer maintenanceId, Set<MaintenanceServiceItem> serviceItems, User user) {
        verifyNotCustomer(user);
        Maintenance maintenance = repository.getById(maintenanceId);
        if (maintenance.getToDate() != null) {
            throw new IllegalEntityException(MAINTENANCE_FINISHED);
        }
        List<MaintenanceServiceItem> maintenanceServices = maintenance.getMaintenanceServices();
        if (!maintenanceServices.isEmpty()) {
            for (MaintenanceServiceItem item : serviceItems) {
                var index = maintenanceServices.indexOf(item);
                if (index >= 0) {
                    //service in the list - set workHours
                    var serviceItem = maintenanceServices.remove(index);
                    serviceItem.setHours(item.getHours());
                    maintenanceServices.add(serviceItem);
                } else {
                    maintenanceServices.add(item);
                }
            }
        } else {
            maintenanceServices.addAll(serviceItems);
        }
        maintenance.setMaintenanceServices(maintenanceServices);
        return repository.update(maintenance);
    }

    @Override
    public Maintenance removeService(Integer maintenanceId, Integer serviceItemId, User user) {
        verifyNotCustomer(user);
        Maintenance maintenance = repository.getById(maintenanceId);

        if (maintenance.getToDate() != null) {
            throw new IllegalEntityException(MAINTENANCE_FINISHED);
        }
        var maintenanceServices = maintenance.getMaintenanceServices();
        MaintenanceServiceItem item = maintenanceServiceRepository.getById(serviceItemId);
        if (!maintenanceServices.contains(item)) {
            throw new IllegalEntityException(SERVICE_NOT_IN_MAINTENANCE);
        }
        maintenanceServices.remove(item);
        maintenance.setMaintenanceServices(maintenanceServices);
        return repository.update(maintenance);
    }

    public String sendPDFByMail(MaintenanceSearchParameters parameters, String currencyAbbreviation) {
        if (parameters.getVehicleId() == null && parameters.getCustomerId() == null) {
            throw new IllegalEntityException("Select Customer or vehicle");
        }
        User user;
        if(parameters.getCustomerId()!= null){
            user = userRepository.getById(parameters.getCustomerId());
        }else{
            var vehicle = vehicleRepository.getById(parameters.getVehicleId());
            user = vehicle.getCustomer();
        }
        String email= user.getEmail();
        return     sendPDFByMail(parameters, currencyAbbreviation, email);
    }

    private String sendPDFByMail(MaintenanceSearchParameters parameters, String currencyAbbreviation, String email) {
        MailSender ms = new MailSender();
        generatePDF(parameters, currencyAbbreviation);
        if (!ms.sendMail(email, SUBJECT_INVOICE, "Invoice", "src/main/resources/pdf/pdfInvoice.pdf")) {
            throw new AuthenticationFailureException(ms.getErrorTrace());
        }
        return "mail sent";
    }

    @Override
    public String generatePDF(MaintenanceSearchParameters parameters, String currencyAbbreviation) throws IllegalEntityException {
        if (parameters.getVehicleId() == null && parameters.getCustomerId() == null) {
            throw new IllegalEntityException("Select Customer or vehicle");
        }
        List<Maintenance> maintenances = repository.getAll(parameters);
        if (maintenances.isEmpty()) {
            throw new EntityNotFoundException("No selected maintenances!");
        }
        if (currencyAbbreviation == null || currencyAbbreviation.isBlank()) {
            currencyAbbreviation = DEFAULT_CURRENCY;
        }
        Currency currency = currencyRepository.getByAbbreviation(currencyAbbreviation);
        return generatePDF(maintenances, currency);
    }

    private String generatePDF(List<Maintenance> maintenances, Currency currency) throws IllegalEntityException {
        PDFiText document = new PDFiText();
        document.createPDF(maintenances, currency);
        return "file generated";
    }
}

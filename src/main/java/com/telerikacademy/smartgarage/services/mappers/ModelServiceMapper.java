package com.telerikacademy.smartgarage.services.mappers;

import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.ServiceDto;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelServiceMapper {
    private final ServiceRepository repository;

    @Autowired
    public ModelServiceMapper(ServiceRepository repository) {
        this.repository = repository;
    }

    public Service fromDto(ServiceDto dto) {
        Service service = new Service();
        dtoToObject(dto, service);
        return service;
    }

    public Service fromDto(ServiceDto dto, int id) {
        Service service = repository.getById(id);
        dtoToObject(dto, service);
        return service;
    }

    private void dtoToObject(ServiceDto dto, Service service) {
        service.setName(dto.getName());
        service.setPricePerHour(dto.getPricePerHour());
        service.setEnabled(true);
    }

    public ServiceDto toDto(Service service) {
        ServiceDto dto = new ServiceDto();
        dto.setName(service.getName());
        dto.setPricePerHour(service.getPricePerHour());
        return dto;
    }
}

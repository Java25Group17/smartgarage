package com.telerikacademy.smartgarage.services.mappers;

import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.repositories.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.smartgarage.models.MaintenanceStatus.*;

@Component
public class ModelMaintenanceMapper {
    private final MaintenanceRepository repository;
    private final MaintenanceStatusRepository statusRepository;
    private final ServiceRepository serviceRepository;
    private final VehicleRepository vehicleRepository;
    private final MaintenanceServiceRepository maintenanceServiceRepository;

    @Autowired
    public ModelMaintenanceMapper(MaintenanceRepository repository,
                                  MaintenanceStatusRepository statusRepository,
                                  ServiceRepository serviceRepository,
                                  VehicleRepository vehicleRepository,
                                  MaintenanceServiceRepository maintenanceServiceRepository) {
        this.repository = repository;
        this.statusRepository = statusRepository;
        this.serviceRepository = serviceRepository;
        this.vehicleRepository = vehicleRepository;
        this.maintenanceServiceRepository = maintenanceServiceRepository;
    }

    public Maintenance fromDto(MaintenanceDto maintenanceDto) {
        Maintenance maintenance = new Maintenance();
        dtoToObject(maintenanceDto, maintenance);
        return maintenance;
    }

    public Maintenance fromDto(MaintenanceDto dto, int id) {
        Maintenance maintenance = repository.getById(id);
        dtoToObject(dto, maintenance);
        return maintenance;
    }

    public MaintenanceDto toDto(Maintenance maintenance) {
        MaintenanceDto dto = new MaintenanceDto();
        dto.setVehicleId(maintenance.getVehicle().getId());
        dto.setServicesItems(maintenance.getMaintenanceServices());
        dto.setToDate(maintenance.getToDate());
        dto.setFromDate(maintenance.getFromDate());
        return dto;
    }

    private void dtoToObject(MaintenanceDto maintenanceDto, Maintenance maintenance) {
        maintenance.setFromDate(maintenanceDto.getFromDate());
        maintenance.setToDate(maintenanceDto.getToDate());
        maintenance.setVehicle(vehicleRepository.getById(maintenanceDto.getVehicleId()));
        maintenance.setMaintenanceServices(maintenanceDto.getServicesItems());
        maintenance.setMaintenanceStatus(generateStatus(maintenanceDto));
    }

    private MaintenanceStatus generateStatus(MaintenanceDto dto) {
        if (dto.getFromDate() == null) {
            return statusRepository.getById(NOT_STARTED);
        } else if (dto.getToDate() == null) {
            return statusRepository.getById(IN_PROGRESS);
        } else return statusRepository.getById(READY_FOR_PICKUP);
    }

    private MaintenanceServiceItem dtoToObject(MaintenanceServiceItemDto dto, MaintenanceServiceItem item) {

        item.setMaintenance(repository.getById(dto.getMaintenanceId()));
        item.setService(serviceRepository.getById(dto.getServiceId()));
        item.setHours(dto.getHours());
        return item;
    }

    public Set<MaintenanceServiceItem> fromDtoList(List<MaintenanceServiceItemDto> serviceItems, Integer maintenanceId) {
        Maintenance maintenance = repository.getById(maintenanceId);
        Set<MaintenanceServiceItem> serviceItemSet = new HashSet<>();
        for (MaintenanceServiceItemDto serviceItemDto : serviceItems) {
            serviceItemSet.add(dtoToObject(serviceItemDto, new MaintenanceServiceItem()));
        }
        return serviceItemSet;
    }

    public MaintenanceSearchParameters fromDto(MaintenanceSearchDto searchDto) {
        MaintenanceSearchParameters parameters = new MaintenanceSearchParameters();
        var startDate = searchDto.getFromDate();
        parameters.setFromDate(startDate == null || startDate.isBlank() ? null : LocalDate.parse(startDate));
        var endDate = searchDto.getToDate();
        parameters.setToDate(endDate == null || endDate.isBlank() ? null : LocalDate.parse(endDate));
        parameters.setOrderByDate(searchDto.getOrderByDate());
        parameters.setStatusId(searchDto.getStatusId());
        parameters.setVehicleId(searchDto.getVehicleId());
        return parameters;
    }

    public MaintenanceServiceItem fromDto(MaintenanceServiceItemDto dto,Integer itemId) {
        MaintenanceServiceItem item = maintenanceServiceRepository.getById(itemId);
        return dtoToObject(dto,item);
    }
    public MaintenanceServiceItemDto toDto(MaintenanceServiceItem item) {
        MaintenanceServiceItemDto dto = new MaintenanceServiceItemDto();
        dto.setMaintenanceId(item.getMaintenance().getId());
        dto.setServiceId(item.getService().getId());
        dto.setHours(item.getHours());
        return dto;
    }

    public MaintenanceServiceItem fromDto(MaintenanceServiceItemDto dto) {
        return dtoToObject(dto,new MaintenanceServiceItem());
    }
}

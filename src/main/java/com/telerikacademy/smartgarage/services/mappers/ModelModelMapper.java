package com.telerikacademy.smartgarage.services.mappers;

import com.telerikacademy.smartgarage.models.Model;
import com.telerikacademy.smartgarage.models.ModelDto;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import org.springframework.stereotype.Component;

@Component
public class ModelModelMapper {
    private final ModelRepository repository;
    private final ManufacturerRepository manufacturerRepository;

    public ModelModelMapper(ModelRepository repository, ManufacturerRepository manufacturerRepository) {
        this.repository = repository;
        this.manufacturerRepository = manufacturerRepository;
    }

    public Model fromDto(ModelDto dto) {
        Model model = new Model();
        dtoToObject(dto, model);
        return model;
    }

    public Model fromDto(ModelDto dto, int id) {
        Model model = repository.getById(id);
        dtoToObject(dto, model);
        return model;
    }

    private void dtoToObject(ModelDto dto, Model model) {
        model.setName(dto.getName());
        var manufacturer = manufacturerRepository.getById(dto.getManufacturerId());
        model.setManufacturer(manufacturer);
    }
}

package com.telerikacademy.smartgarage.services.mappers;

import com.telerikacademy.smartgarage.models.VehicleCategory;
import com.telerikacademy.smartgarage.models.VehicleCategoryDto;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelVehicleCategoryMapper {

    private final VehicleCategoryRepository categoryRepository;

    @Autowired
    public ModelVehicleCategoryMapper(VehicleCategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public VehicleCategory fromDto(VehicleCategoryDto categoryDto) {
        VehicleCategory category = new VehicleCategory();
        dtoToObject(categoryDto, category);
        return category;
    }

    public VehicleCategory fromDto(VehicleCategoryDto categoryDto, int id) {
        VehicleCategory category = categoryRepository.getById(id);
        dtoToObject(categoryDto, category);
        return category;
    }

    private void dtoToObject(VehicleCategoryDto categoryDto, VehicleCategory category) {
        category.setName(categoryDto.getName());
    }
}

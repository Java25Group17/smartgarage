package com.telerikacademy.smartgarage.services.mappers;

import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.VehicleCreateDto;
import com.telerikacademy.smartgarage.models.VehicleDto;
import com.telerikacademy.smartgarage.repositories.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelVehicleMapper {
    private final VehicleRepository repository;
    private final UserRepository userRepository;
    private final VehicleCategoryRepository vehicleCategoryRepository;
    private final EngineTypeRepository engineTypeRepository;
    private final ModelRepository modelRepository;


    @Autowired
    public ModelVehicleMapper(VehicleRepository repository, UserRepository userRepository, VehicleCategoryRepository vehicleCategoryRepository, EngineTypeRepository engineTypeRepository, ModelRepository modelRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.vehicleCategoryRepository = vehicleCategoryRepository;
        this.engineTypeRepository = engineTypeRepository;
        this.modelRepository = modelRepository;
    }

    public Vehicle fromDto(VehicleDto dto) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(dto, vehicle);
        return vehicle;
    }

    public Vehicle fromDto(VehicleDto dto, int id) {
        Vehicle vehicle = repository.getById(id);
        dtoToObject(dto, vehicle);
        return vehicle;
    }

    public Vehicle fromCreateDto(VehicleCreateDto dto) {
        Vehicle vehicle = new Vehicle();
        createDtoToObject(dto, vehicle);
        return vehicle;
    }

    private void dtoToObject(VehicleDto dto, Vehicle vehicle) {
        vehicle.setCustomer(userRepository.getById(dto.getCustomerId()));
        vehicle.setCategory(vehicleCategoryRepository.getById(dto.getVehicleCategoryId()));
        vehicle.setColour(dto.getColour().toLowerCase());
        vehicle.setEngineType(engineTypeRepository.getById(dto.getEngineTypeId()));
        vehicle.setRegistrationPlate(dto.getRegistrationPlate());
        vehicle.setIdentificationNumber(dto.getIdentificationNumber());
        vehicle.setModel(modelRepository.getById(dto.getModelId()));
        vehicle.setEnabled(true);
    }

    public VehicleDto toDto(Vehicle vehicle) {
        VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setCustomerId(vehicle.getId());
        vehicleDto.setVehicleCategoryId(vehicle.getCategory().getId());
        vehicleDto.setColour(vehicle.getColour().toLowerCase());
        vehicleDto.setEngineTypeId(vehicle.getEngineType().getId());
        vehicleDto.setRegistrationPlate(vehicle.getRegistrationPlate());
        vehicleDto.setIdentificationNumber(vehicle.getIdentificationNumber());
        vehicleDto.setModelId(vehicle.getModel().getId());
        vehicleDto.setEnabled(true);

        return vehicleDto;
    }

    private void createDtoToObject(VehicleCreateDto dto, Vehicle vehicle) {
        vehicle.setCustomer(userRepository.getById(dto.getCustomerId()));
        vehicle.setCategory(vehicleCategoryRepository.getById(dto.getVehicleCategoryId()));
        vehicle.setColour(dto.getColour().toLowerCase());
        vehicle.setEngineType(engineTypeRepository.getById(dto.getEngineTypeId()));
        vehicle.setRegistrationPlate(dto.getRegistrationPlate());
        vehicle.setIdentificationNumber(dto.getIdentificationNumber());
        vehicle.setEnabled(true);
    }
}

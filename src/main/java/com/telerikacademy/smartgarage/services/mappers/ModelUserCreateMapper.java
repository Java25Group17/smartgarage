package com.telerikacademy.smartgarage.services.mappers;

import com.telerikacademy.smartgarage.models.Role;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.UserCreateDto;
import com.telerikacademy.smartgarage.repositories.contracts.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelUserCreateMapper {
    private final RoleRepository roleRepository;

    @Autowired
    public ModelUserCreateMapper(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public User fromDto(UserCreateDto userCreateDto) {
        User user = new User();
        dtoToObject(userCreateDto, user);
        return user;
    }

    private void dtoToObject(UserCreateDto userCreateDto, User user) {
        user.setFirstName(userCreateDto.getFirstName());
        user.setLastName(userCreateDto.getLastName());
        user.setEmail(userCreateDto.getEmail());
        user.setPhone(userCreateDto.getPhone());
        Role role = roleRepository.getByName("Customer");
        user.setRole(role);
        user.setEnabled(true);
    }
}

package com.telerikacademy.smartgarage.services.mappers;

import com.telerikacademy.smartgarage.models.UpdatePasswordDto;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.UserUpdateDto;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelUserUpdateMapper {
    private final UserRepository userRepository;

    @Autowired
    public ModelUserUpdateMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User fromDto(UserUpdateDto userUpdateDto) {
        User user = new User();
        dtoToObject(userUpdateDto, user);
        return user;
    }

    public User fromDto(UserUpdateDto userUpdateDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userUpdateDto, user);
        return user;
    }

    public User fromDto(UpdatePasswordDto updatePassword, int id) {
        User user = userRepository.getById(id);
        dtoToObject(updatePassword, user);
        return user;
    }

    private void dtoToObject(UserUpdateDto userUpdateDto, User user) {
        user.setEmail(userUpdateDto.getEmail());
        user.setFirstName(userUpdateDto.getFirstName());
        user.setLastName(userUpdateDto.getLastName());
        user.setPhone(userUpdateDto.getPhone());
    }

    private void dtoToObject(UpdatePasswordDto updatePassword, User user) {
        user.setEmail(updatePassword.getEmail());
        user.setPassword(updatePassword.getPassword());
    }

    public UserUpdateDto toDto(User user) {
        UserUpdateDto userDto = new UserUpdateDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPhone(user.getPhone());
        return userDto;
    }
}

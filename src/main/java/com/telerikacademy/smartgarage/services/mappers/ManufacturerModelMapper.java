package com.telerikacademy.smartgarage.services.mappers;

import com.telerikacademy.smartgarage.models.Manufacturer;
import com.telerikacademy.smartgarage.models.ManufacturerDto;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import org.springframework.stereotype.Component;

@Component
public class ManufacturerModelMapper {
    private final ManufacturerRepository repository;

    public ManufacturerModelMapper(ManufacturerRepository repository) {
        this.repository = repository;
    }

    public Manufacturer fromDto(ManufacturerDto dto) {
        Manufacturer manufacturer = new Manufacturer();
        dtoToObject(dto, manufacturer);
        return manufacturer;
    }

    public Manufacturer fromDto(ManufacturerDto dto, int id) {
        Manufacturer manufacturer = repository.getById(id);
        dtoToObject(dto, manufacturer);
        return manufacturer;
    }

    private void dtoToObject(ManufacturerDto dto, Manufacturer manufacturer) {
        manufacturer.setName(dto.getName());
    }
}

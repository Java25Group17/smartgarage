package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.VehicleSearchParameters;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyNotCustomer;
import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyUserHasRoles;

@Service
public class VehicleServiceImpl implements VehicleService {
    private final VehicleRepository repository;
    private final MaintenanceRepository maintenanceRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository repository,
                              MaintenanceRepository maintenanceRepository) {
        this.repository = repository;
        this.maintenanceRepository = maintenanceRepository;
    }

    @Override
    public Vehicle getById(Integer id) {
        return repository.getById(id);
    }

    @Override
    public List<Vehicle> getAll(User user, VehicleSearchParameters parameters) {
     //   verifyNotCustomer(user);
        if(!parameters.isEnabled()){
            verifyUserHasRoles(user, "Only for Admin!", UserRoles.ADMIN);
        }
        return repository.getAll(parameters);
    }

    @Override
    public Vehicle create(Vehicle objectToCreate, User user) {
        verifyNotCustomer(user);
        boolean duplicateExists = true;
        try {
            Vehicle vehicleObj = repository.getByIdentificationNumber(objectToCreate.getIdentificationNumber());
            if (!vehicleObj.isEnabled()) {
                return enable(vehicleObj, objectToCreate);
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Vehicle", "identification number", objectToCreate.getIdentificationNumber());
        }
        repository.create(objectToCreate);
        return objectToCreate;
    }

    @Override
    public Vehicle update(Vehicle objectToUpdate, User user) {
        verifyNotCustomer(user);
        repository.getById(objectToUpdate.getId());
        repository.update(objectToUpdate);
        return objectToUpdate;
    }

    @Override
    public void delete(Integer id, User userAuthenticator) {
        verifyNotCustomer(userAuthenticator);
        var inUse = maintenanceRepository.selectCountVehicleInMaintenance(id);
        if (inUse != null && inUse.intValue()>0) {
            throw new IllegalEntityException("Vehicle can't be deleted! The vehicle is in maintenance at the moment.");
        }
        Vehicle existingObject = repository.getById(id);
        existingObject.setEnabled(false);
        repository.update(existingObject);
    }

    private Vehicle enable(Vehicle existingObj, Vehicle newObj) {
        existingObj.setEnabled(true);
        existingObj.setCustomer(newObj.getCustomer());
        existingObj.setModel(newObj.getModel());
        existingObj.setColour(newObj.getColour());
        existingObj.setCategory(newObj.getCategory());
        existingObj.setRegistrationPlate(existingObj.getRegistrationPlate());
        existingObj.setEngineType(newObj.getEngineType());
        return repository.update(existingObj);
    }
}

package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.ServiceSearchParameters;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceServiceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import com.telerikacademy.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyNotCustomer;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {
    private final ServiceRepository repository;
    private final MaintenanceServiceRepository maintenanceServiceRepository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository repository, MaintenanceServiceRepository maintenanceServiceRepository) {
        this.repository = repository;
        this.maintenanceServiceRepository = maintenanceServiceRepository;
    }

    @Override
    public List<Service> getAll(ServiceSearchParameters parameters) {
        return repository.getAll(parameters);
    }

    @Override
    public Service getById(Integer id) {
        return repository.getById(id);
    }

    @Override
    public Service create(Service objectToCreate, User user) {
        verifyNotCustomer(user);
        boolean duplicateExists = true;
        try {
            Service serviceObj = repository.getByName(objectToCreate.getName());
            if (!serviceObj.isEnabled()) {
                return enable(serviceObj, objectToCreate);
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Service", "name", objectToCreate.getName());
        }
        return repository.create(objectToCreate);
    }

    @Override
    public Service update(Service objectToUpdate, User user) {
        verifyNotCustomer(user);
        boolean duplicateExists = true;
        Service existingObject = repository.getByName(objectToUpdate.getName());
        if (existingObject.getId() == objectToUpdate.getId())
            duplicateExists = false;

        if (duplicateExists) {
            throw new DuplicateEntityException("Service", "name", objectToUpdate.getName());
        }
        repository.update(objectToUpdate);
        return objectToUpdate;
    }

    /**
     * There is no deletion, instead we make soft delete - disable
     **/
    @Override
    public void delete(Integer id, User user) {
        verifyNotCustomer(user);
        var inUse = maintenanceServiceRepository.selectCountServiceInUse(id);
        if (inUse != null && inUse.intValue() > 0) {
            throw new IllegalEntityException("Service can't be deleted! There are cars that are using it at the moment.");
        }
        Service existingObject = repository.getById(id);
        existingObject.setEnabled(false);
        repository.update(existingObject);
    }

    private Service enable(Service existingObj, Service newObj) {
        existingObj.setEnabled(true);
        existingObj.setPricePerHour(newObj.getPricePerHour());
        return repository.update(existingObj);
    }
}

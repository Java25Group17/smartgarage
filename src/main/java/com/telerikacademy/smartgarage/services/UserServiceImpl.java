package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.controllers.AuthenticationHelper;
import com.telerikacademy.smartgarage.exceptions.*;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.UserSearchParameters;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.smartgarage.services.contracts.UserService;
import com.telerikacademy.smartgarage.utils.MailSender;
import com.telerikacademy.smartgarage.utils.MyUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import static com.telerikacademy.smartgarage.controllers.AuthenticationHelper.generatePasswordHash;
import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyUserHasRoles;

@Service
public class UserServiceImpl implements UserService {
    private static final String CUSTOMER_ERROR_MESSAGE = "Only owner, employee or admin can perform the operation.";
    private static final String SUBJECT_NEW_PASSWORD = "Welcome to SmartGarage.G17";
    private static final String INCORRECT_VALUE_ORDER = "Value for order should be 'name' or 'date'.";

    private final UserRepository userRepository;
    private final MaintenanceRepository maintenanceRepository;
    private final VehicleRepository vehicleRepository;
    private final AuthorisationHelper authorisationHelper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           MaintenanceRepository maintenanceRepository,
                           VehicleRepository vehicleRepository,
                           AuthorisationHelper authorisationHelper) {
        this.userRepository = userRepository;
        this.maintenanceRepository = maintenanceRepository;
        this.vehicleRepository = vehicleRepository;
        this.authorisationHelper = authorisationHelper;
    }

    @Override
    public List<User> getAll(User userAuthenticator) {
        verifyUserHasRoles(userAuthenticator, "", UserRoles.EMPLOYEE, UserRoles.ADMIN);
        return userRepository.getAll();
    }

    @Override
    public List<User> filterByMultiple(UserSearchParameters searchParameters, User userAuthenticator) {
        verifyUserHasRoles(userAuthenticator, "", UserRoles.EMPLOYEE, UserRoles.ADMIN);

        if (verifyUserHasRoles(userAuthenticator, UserRoles.EMPLOYEE)) {
            searchParameters.setEnabled("true");
        }

        if (searchParameters.getOrderBy() != null &&
                !searchParameters.getOrderBy().isBlank() &&
                !searchParameters.getOrderBy().equalsIgnoreCase("-1") &&
                !searchParameters.getOrderBy().equalsIgnoreCase("name") &&
                !searchParameters.getOrderBy().equalsIgnoreCase("date")) {
            throw new IllegalEntityException(INCORRECT_VALUE_ORDER);
        }

        return userRepository.filterByMultiple(searchParameters);
    }

    @Override
    public User getById(Integer id, User userAuthenticator) {
   //     verifyUserHasRoles(userAuthenticator, UserRoles.CUSTOMER);
        return userRepository.getById(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User create(User user, User userAuthenticator) {
        verifyUserHasRoles(userAuthenticator, UserRoles.ADMIN, UserRoles.EMPLOYEE);

        boolean duplicateUserExists = true;
        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateUserExists = false;
        }

        if (duplicateUserExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        MyUUID myUUID = new MyUUID();
        String password = myUUID.getUUID().substring(0, 8);
        user.setPassword(generatePasswordHash(password));

        MailSender ms = new MailSender();
        if (!ms.sendMail(user.getEmail(), SUBJECT_NEW_PASSWORD, password)) {
            throw new AuthenticationFailureException(ms.getErrorTrace());
        }

        return userRepository.create(user);
    }

    @Override
    public User update(User user, User userAuthenticator) {
        verifyUserHasRoles(userAuthenticator, UserRoles.ADMIN, UserRoles.EMPLOYEE);

        boolean duplicateExists = true;
        User existingUser;
        try {
            existingUser = userRepository.getByEmail(user.getEmail());
            if (existingUser.getId() == user.getId())
                duplicateExists = false;
        } catch (EntityNotFoundException e) {
            // user wants to change the email with new - not in the system
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        return userRepository.update(user);
    }

    /*There is no deletion, instead we make soft delete - disable*/
    @Override
    public void delete(Integer id, User userAuthenticator) {
        verifyUserHasRoles(userAuthenticator, UserRoles.ADMIN, UserRoles.EMPLOYEE);
        User existingObject = userRepository.getById(id);
        var vehiclesForClient = maintenanceRepository.selectCountVehicleForClient(id);
        if (vehiclesForClient == null || vehiclesForClient == 0) {
            vehicleRepository.disableVehicles(id);
            existingObject.setEnabled(false);
            userRepository.update(existingObject);
        }
    }

    @Override
    public String resetPassword(String email) {
        String returnString = "Detailed password reset instructions have been sent to your email!";
        String subject = "Reset Password";

        User user = userRepository.getByEmail(email);

        StringBuilder timeStamp = new StringBuilder();
        timeStamp.append(new SimpleDateFormat("yyyyMMddHHmm").format(new Date()));
        timeStamp.append(email);
        System.out.println("timeStamp: " + timeStamp);

        String encodedString = Base64.getEncoder().encodeToString(timeStamp.toString().getBytes());

        StringBuilder link = new StringBuilder();
        link.append("http://localhost:8080/users/check/");
        link.append(encodedString);

        MailSender ms = new MailSender();
        if (!ms.sendMail(user, subject, link.toString())) {
            throw new AuthenticationFailureException(ms.getErrorTrace());
        }

        return returnString;
    }

    @Override
    public String resetPasswordCheckToken(String token) {
        String negativeReturnTokenExpired = "Възникна грешка - изминали са повече от 24 часа " +
                "от генерирането на линка за ресетване на паролата!";

        byte[] decodedBytes = Base64.getDecoder().decode(token);
        String decodedString = new String(decodedBytes);
        Date tokenDate = null;

        try {
            tokenDate = new SimpleDateFormat("yyyyMMddHHmm").
                    parse(decodedString.substring(0, 12));

            LocalDateTime ldtToken = LocalDateTime.ofInstant(
                    tokenDate.toInstant(), ZoneId.systemDefault());

            LocalDateTime ldtNow = LocalDateTime.ofInstant(
                    (new Date()).toInstant(), ZoneId.systemDefault());

            Duration dDiff = Duration.between(ldtToken, ldtNow);
            long diff = Math.abs(dDiff.toMinutes());

            // 24 hours * 60 minutes = 1440 minutes
            if (diff > 1440)
                return negativeReturnTokenExpired;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String email = decodedString.substring(12);
        userRepository.getByEmail(email);
        return "Всичко е наред - пускаме формата за въвеждане на новата парола...";
    }

    @Override
    public String resetPasswordSetNewPassword(String email, String password, String passwordConfirm) {
        String returnString = "Паролата беше обновена успешно!";
        String passwordsNotEqual = "Възникна грешка - паролите не са еднакви!";

        User user = userRepository.getByEmail(email);

        if (!password.equals(passwordConfirm)) {
            throw new AuthenticationFailureException(passwordsNotEqual);
        }

        String passwordHash = AuthenticationHelper.generatePasswordHash(password);
        user.setPassword(passwordHash);
        userRepository.update(user);

        return returnString;
    }
}

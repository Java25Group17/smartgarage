package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.VehicleCategory;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleCategoryRepository;
import com.telerikacademy.smartgarage.services.contracts.VehicleCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyNotCustomer;

@Service
public class VehicleCategoryServiceImpl implements VehicleCategoryService {

    private final VehicleCategoryRepository repository;

    @Autowired
    public VehicleCategoryServiceImpl(VehicleCategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<VehicleCategory> getAll() {
        return repository.getAll();
    }

    @Override
    public VehicleCategory getById(Integer id) {
        return repository.getById(id);
    }

    @Override
    public VehicleCategory create(VehicleCategory category, User user) {
        verifyNotCustomer(user);
        boolean duplicateExists = true;
        try {
            repository.getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("VehicleCategory", "name", category.getName());
        }

        repository.create(category);
        return category;
    }

    @Override
    public VehicleCategory update(VehicleCategory category, User user) {
        verifyNotCustomer(user);
        repository.getByName(category.getName());
        repository.update(category);
        return category;
    }
}

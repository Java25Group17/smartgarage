package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.models.EngineType;
import com.telerikacademy.smartgarage.repositories.contracts.EngineTypeRepository;
import com.telerikacademy.smartgarage.services.contracts.EngineTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EngineTypeServiceImpl implements EngineTypeService {

    private final EngineTypeRepository repository;

    @Autowired
    public EngineTypeServiceImpl(EngineTypeRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<EngineType> getAll() {
        return repository.getAll();
    }

    @Override
    public EngineType getById(Integer id) {
        return repository.getById(id);
    }
}

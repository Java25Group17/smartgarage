package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.models.MaintenanceStatus;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceStatusRepository;
import com.telerikacademy.smartgarage.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    private final MaintenanceStatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(MaintenanceStatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<MaintenanceStatus> getAll() {
        return statusRepository.getAll();
    }
}
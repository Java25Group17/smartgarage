package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

import static com.telerikacademy.smartgarage.utils.Constants.UNAUTHORISED;

@Component
public class AuthorisationHelper {
    private static final String USER_ERROR_MESSAGE = "Only employee or admin can perform the operation.";
    private static final String CUSTOMER_ERROR_MESSAGE = "Only the owner can perform the operation.";

    public AuthorisationHelper() {
    }

    public static boolean verifyUserHasRoles(User users, UserRoles... wantedRoles) {
        return userHasRoles(users, wantedRoles);
    }

    private static boolean userHasRoles(User user, UserRoles... wantedRoles){
        var wantedRolesSet = Arrays.stream(wantedRoles).
                map(UserRoles::toString).
                collect(Collectors.toSet());

        return !wantedRolesSet.stream()
                .filter(role -> role.equals(user.getRole().getName().toLowerCase())).findAny().isEmpty();
    }

    public static void verifyUserHasRoles(User user, String message, UserRoles... wantedRoles) {
        if(!userHasRoles(user, wantedRoles)){
            throw new UnauthorizedOperationException(message);
        }
    }

    public static void verifyNotCustomer(User user) {
        verifyUserHasRoles(user, UNAUTHORISED, UserRoles.ADMIN, UserRoles.EMPLOYEE);
    }
}

package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Model;
import com.telerikacademy.smartgarage.models.ModelSearchParameters;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import com.telerikacademy.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyNotCustomer;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository repository;

    @Autowired
    public ModelServiceImpl(ModelRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Model> getAll(ModelSearchParameters modelSearchParameters) {
        return repository.getAll(modelSearchParameters);
    }

    @Override
    public Model getById(Integer id) {
        return repository.getById(id);
    }

    @Override
    public Model create(Model objectToCreate, User user) {
        verifyNotCustomer(user);
        boolean duplicateExists = true;
        try {
            repository.getByName(objectToCreate.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Model", "name", objectToCreate.getName());
        }
        repository.create(objectToCreate);
        return objectToCreate;
    }

    @Override
    public Model update(Model objectToUpdate, User user) {
        verifyNotCustomer(user);
        repository.getByName(objectToUpdate.getName());
        repository.update(objectToUpdate);
        return objectToUpdate;
    }
}

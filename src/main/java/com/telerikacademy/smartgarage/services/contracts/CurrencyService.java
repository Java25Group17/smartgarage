package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.User;

import java.util.List;

public interface CurrencyService {
    List<Currency> getAll(User userAuthenticator);

    Currency getById(Integer id, User userAuthenticator);

    void update ();
}

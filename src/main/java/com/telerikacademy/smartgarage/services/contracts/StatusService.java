package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.MaintenanceStatus;

import java.util.List;

public interface StatusService {
    List<MaintenanceStatus> getAll();
}

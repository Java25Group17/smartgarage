package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Manufacturer;
import com.telerikacademy.smartgarage.models.ManufacturerSearchParameters;
import com.telerikacademy.smartgarage.models.User;

import java.util.List;

public interface ManufacturerService {
    Manufacturer getById(Integer id);

    List<Manufacturer> getAll(ManufacturerSearchParameters searchParameters);

    Manufacturer create(Manufacturer objToCreate, User userAuthenticator);

    Manufacturer update(Manufacturer manufacturerObj, User userAuthenticator);
}

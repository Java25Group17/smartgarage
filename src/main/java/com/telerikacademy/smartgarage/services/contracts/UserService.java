package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.UserSearchParameters;

import java.util.List;

public interface UserService {

    List<User> getAll(User user);

    List<User> filterByMultiple(UserSearchParameters searchParameter, User user);

    User getById(Integer id, User userAuthenticator);

    User getByEmail(String email);

    User create(User user, User userAuthenticator);

    User update(User user, User userAuthenticator);

    void delete(Integer id, User user);

    String resetPassword(String email);

    String resetPasswordCheckToken(String token);

    String resetPasswordSetNewPassword(String email, String password, String passwordConfirm);
}

package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.EngineType;

import java.util.List;

public interface EngineTypeService {

    EngineType getById(Integer id);

    List<EngineType> getAll();
}

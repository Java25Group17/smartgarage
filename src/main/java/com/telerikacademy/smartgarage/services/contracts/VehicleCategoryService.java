package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.VehicleCategory;

import java.util.List;

public interface VehicleCategoryService {
    VehicleCategory getById(Integer id);

    List<VehicleCategory> getAll();

    VehicleCategory create(VehicleCategory objectToCreate, User user);

    VehicleCategory update(VehicleCategory objectToUpdate, User user);
}

package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Model;
import com.telerikacademy.smartgarage.models.ModelSearchParameters;
import com.telerikacademy.smartgarage.models.User;

import java.util.List;

public interface ModelService {

    Model getById(Integer id);

    List<Model> getAll(ModelSearchParameters modelSearchParameters);

    Model create(Model objToCreate, User userAuthenticator);

    Model update(Model objToCreate, User userAuthenticator);
}

package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.Vehicle;
import com.telerikacademy.smartgarage.models.VehicleSearchParameters;

import java.util.List;

public interface VehicleService {

    Vehicle getById(Integer id);

    List<Vehicle> getAll(User user, VehicleSearchParameters parameters);

    Vehicle create(Vehicle objectToCreate, User user);

    Vehicle update(Vehicle objectToUpdate, User user);

    void delete(Integer id, User userAuthenticator);
}

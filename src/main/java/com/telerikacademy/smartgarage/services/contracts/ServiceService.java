package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Service;
import com.telerikacademy.smartgarage.models.ServiceSearchParameters;
import com.telerikacademy.smartgarage.models.User;

import java.util.List;

public interface ServiceService {

    Service getById(Integer id);

    List<Service> getAll(ServiceSearchParameters parameters);

    Service create(Service objectToCreate, User user);

    Service update(Service objectToUpdate, User user);

    void delete(Integer id, User userAuthenticator);
}

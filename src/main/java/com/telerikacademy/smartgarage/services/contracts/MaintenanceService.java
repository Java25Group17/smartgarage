package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Maintenance;
import com.telerikacademy.smartgarage.models.MaintenanceSearchParameters;
import com.telerikacademy.smartgarage.models.MaintenanceServiceItem;
import com.telerikacademy.smartgarage.models.User;

import java.util.List;
import java.util.Set;

public interface MaintenanceService {

    List<Maintenance> getAll(User user, MaintenanceSearchParameters parameters);

    Maintenance getById(Integer id, User user);

    Maintenance create(User user, Maintenance objToCreate);

    Maintenance update(Maintenance maintenance, User user);

    Maintenance removeService(Integer maintenanceId, Integer serviceId, User user);

    Maintenance addServices(Integer id, Set<MaintenanceServiceItem> itemSet, User user);

    String generatePDF(MaintenanceSearchParameters maintenanceSearchParameters, String currency);

    String sendPDFByMail(MaintenanceSearchParameters parameters, String currencyAbbreviation);
}

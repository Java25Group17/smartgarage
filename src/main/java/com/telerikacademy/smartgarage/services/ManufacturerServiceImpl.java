package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Manufacturer;
import com.telerikacademy.smartgarage.models.ManufacturerSearchParameters;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.smartgarage.services.contracts.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyNotCustomer;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {
    private final ManufacturerRepository repository;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Manufacturer> getAll(ManufacturerSearchParameters parameters) {
        return repository.getAll(parameters);
    }

    @Override
    public Manufacturer getById(Integer id) {
        return repository.getById(id);
    }

    @Override
    public Manufacturer create(Manufacturer objectToCreate, User user) {
        verifyNotCustomer(user);
        boolean duplicateExists = true;
        try {
            repository.getByName(objectToCreate.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Manufacturer", "name", objectToCreate.getName());
        }
        repository.create(objectToCreate);
        return objectToCreate;
    }

    @Override
    public Manufacturer update(Manufacturer objectToUpdate, User user) {
        verifyNotCustomer(user);
        repository.getByName(objectToUpdate.getName());
        repository.update(objectToUpdate);
        return objectToUpdate;
    }
}

package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.CurrentDate;
import com.telerikacademy.smartgarage.models.User;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import com.telerikacademy.smartgarage.repositories.contracts.CurrencyRepository;
import com.telerikacademy.smartgarage.services.contracts.CurrencyService;
import com.telerikacademy.smartgarage.utils.ExchangeRates_DOM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static com.telerikacademy.smartgarage.services.AuthorisationHelper.verifyUserHasRoles;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    private static final String CUSTOMER_ERROR_MESSAGE = "Only owner, employee or admin can perform the operation.";
    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public List<Currency> getAll(User userAuthenticator) {
        verifyUserHasRoles(userAuthenticator, UserRoles.ADMIN, UserRoles.EMPLOYEE);
        return currencyRepository.getAll();
    }

    @Override
    public Currency getById(Integer id, User userAuthenticator) {
        return currencyRepository.getById(id);
    }

    @Override
    public void update() {
        List<Currency> currencies = currencyRepository.getAll();
        CurrentDate currentDate = currencyRepository.getDate();
        if (!currentDate.getDate().equals(LocalDate.now())) {
            ExchangeRates_DOM exchangeRates_dom = new ExchangeRates_DOM(currencies);
            Map<String, Double> mapExchangeRatesDOM = exchangeRates_dom.getExchangeRates();
            if (!mapExchangeRatesDOM.isEmpty()) {
                for (Currency currency : currencies) {
                    if (mapExchangeRatesDOM.containsKey(currency.getAbbreviation())) {
                        currency.setDefaultExchangeCourse(
                                mapExchangeRatesDOM.get(currency.getAbbreviation()));
                        currencyRepository.update(currency);
                    }
                }
                currentDate.setDate(LocalDate.now());
                currencyRepository.updateDate(currentDate);
            }
        }
    }
}

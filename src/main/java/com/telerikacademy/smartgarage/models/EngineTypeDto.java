package com.telerikacademy.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.telerikacademy.smartgarage.utils.Constants.NAME_CANT_BE_EMPTY;

public class EngineTypeDto {
    @NotNull(message = NAME_CANT_BE_EMPTY)
    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 symbols.")
    private String name;

    public EngineTypeDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

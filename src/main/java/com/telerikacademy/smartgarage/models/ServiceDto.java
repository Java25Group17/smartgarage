package com.telerikacademy.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import static com.telerikacademy.smartgarage.utils.Constants.NAME_CANT_BE_EMPTY;

public class ServiceDto {

    @NotNull(message = NAME_CANT_BE_EMPTY)
    @Size(min = 2, max = 100, message = "Name should be between 2 and 100 symbols.")
    private String name;

    @Positive(message = "Price should be positive number")
    private double pricePerHour;

    public ServiceDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }
}

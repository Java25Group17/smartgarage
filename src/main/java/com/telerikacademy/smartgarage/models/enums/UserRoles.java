package com.telerikacademy.smartgarage.models.enums;

import java.util.Locale;

public enum UserRoles {
    EMPLOYEE, CUSTOMER, ADMIN;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}

package com.telerikacademy.smartgarage.models.enums;

public enum MaintenanceStatusEnum {
    IN_PROGRESS, NOT_STARTED, READY_FOR_PICKUP;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}

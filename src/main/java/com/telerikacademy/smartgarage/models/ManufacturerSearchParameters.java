package com.telerikacademy.smartgarage.models;

public class ManufacturerSearchParameters {

    private String name;

    public ManufacturerSearchParameters() {
    }

    public ManufacturerSearchParameters(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

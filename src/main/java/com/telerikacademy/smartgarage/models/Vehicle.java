package com.telerikacademy.smartgarage.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "vehicles")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    @JoinColumn(name = "customer_id")
    private User customer;

    @Column(name = "registration_plate")
    private String registrationPlate;

    @Column(name = "identification_number")
    private String identificationNumber;

    @OneToOne
    @JoinColumn(name = "model_id")
    private Model model;

    @OneToOne
    @JoinColumn(name = "category_id")
    private VehicleCategory category;

    @Column(name = "colour")
    private String colour;

    @OneToOne
    @JoinColumn(name = "engine_type_id")
    private EngineType engineType;

    @Column(name = "enabled")
    private boolean enabled;

    public Vehicle() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public String getRegistrationPlate() {
        return registrationPlate;
    }

    public void setRegistrationPlate(String registrationPlate) {
        this.registrationPlate = registrationPlate;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public VehicleCategory getCategory() {
        return category;
    }

    public void setCategory(VehicleCategory category) {
        this.category = category;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public EngineType getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineType engineType) {
        this.engineType = engineType;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return getCustomer().equals(vehicle.getCustomer()) &&
                getRegistrationPlate().equals(vehicle.getRegistrationPlate()) &&
                getIdentificationNumber().equals(vehicle.getIdentificationNumber()) &&
                getModel().equals(vehicle.getModel()) &&
                getCategory().equals(vehicle.getCategory()) &&
                getColour().equals(vehicle.getColour()) &&
                getEngineType().equals(vehicle.getEngineType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCustomer(),
                getRegistrationPlate(),
                getIdentificationNumber(),
                getModel(),
                getCategory(),
                getColour(),
                getEngineType());
    }
}

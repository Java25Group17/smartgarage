package com.telerikacademy.smartgarage.models;

public class MaintenanceSearchDto {
    private Integer statusId;
    private Integer vehicleId;
    private String fromDate;
    private String toDate;
    private String orderByDate;
    private String currencyAbbr;

    public MaintenanceSearchDto(Integer statusId,
                                Integer vehicleId,
                                String fromDate,
                                String toDate,
                                String orderByDate) {
        this.statusId = statusId;
        this.vehicleId = vehicleId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.orderByDate = orderByDate;
    }

    public MaintenanceSearchDto() {

    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getOrderByDate() {
        return orderByDate;
    }

    public void setOrderByDate(String orderByDate) {
        this.orderByDate = orderByDate;
    }

    public String getCurrencyAbbr() {
        return currencyAbbr;
    }

    public void setCurrencyAbbr(String currencyAbbr) {
        this.currencyAbbr = currencyAbbr;
    }
}

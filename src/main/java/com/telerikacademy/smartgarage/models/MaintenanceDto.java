package com.telerikacademy.smartgarage.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

public class MaintenanceDto {
    @NotNull
    private Integer vehicleId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate fromDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate toDate;

    private List<MaintenanceServiceItem> servicesItems;

    public MaintenanceDto() {
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public List<MaintenanceServiceItem> getServicesItems() {
        return servicesItems;
    }

    public void setServicesItems(List<MaintenanceServiceItem> servicesItems) {
        this.servicesItems = servicesItems;
    }
}

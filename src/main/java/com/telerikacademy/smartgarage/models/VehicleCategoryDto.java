package com.telerikacademy.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class VehicleCategoryDto {
    @NotNull(message = "Name cant be empty.")
    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 symbols.")
    private String name;

    public VehicleCategoryDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

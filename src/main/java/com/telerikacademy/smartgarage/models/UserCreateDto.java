package com.telerikacademy.smartgarage.models;

import javax.validation.constraints.NotNull;

public class UserCreateDto extends UserUpdateDto{
    @NotNull
    private String role;

    public UserCreateDto() {
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}

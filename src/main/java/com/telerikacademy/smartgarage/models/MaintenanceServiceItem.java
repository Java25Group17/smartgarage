package com.telerikacademy.smartgarage.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "maintenance_services")
public class MaintenanceServiceItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "maintenance_id")
    private Maintenance maintenance;

    @OneToOne
    @JoinColumn(name= "service_id")
    private Service service;

    @Column
    private Integer hours;

    public MaintenanceServiceItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Maintenance getMaintenance() {
        return maintenance;
    }

    public void setMaintenance(Maintenance maintenance) {
        this.maintenance = maintenance;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MaintenanceServiceItem that = (MaintenanceServiceItem) o;
        return getMaintenance().getId() == that.getMaintenance().getId() &&
                getService().getId()== that.getService().getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMaintenance(), getService());
    }
}


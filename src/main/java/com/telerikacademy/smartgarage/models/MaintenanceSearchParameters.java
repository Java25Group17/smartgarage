package com.telerikacademy.smartgarage.models;

import java.time.LocalDate;

public class MaintenanceSearchParameters {
    private Integer statusId;
    private Integer customerId;
    private Integer vehicleId;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String orderByDate;

    public MaintenanceSearchParameters(Integer statusId,
                                       Integer customerId,
                                       Integer vehicleId,
                                       LocalDate fromDate,
                                       LocalDate toDate,
                                       String orderByDate) {
        this.statusId = statusId;
        this.customerId = customerId;
        this.vehicleId = vehicleId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.orderByDate = orderByDate;
    }

    public MaintenanceSearchParameters() {

    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getOrderByDate() {
        return orderByDate;
    }

    public void setOrderByDate(String orderByDate) {
        this.orderByDate = orderByDate;
    }
}

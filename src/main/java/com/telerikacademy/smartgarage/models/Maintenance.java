package com.telerikacademy.smartgarage.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "maintenances")
public class Maintenance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    @Column(name = "from_date")
    private LocalDate fromDate;

    @Column(name = "to_date")
    private LocalDate toDate;

    @OneToOne
    @JoinColumn(name = "status_id")
    private MaintenanceStatus maintenanceStatus;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "maintenance", orphanRemoval=true)
    private List<MaintenanceServiceItem> maintenanceServices;

    public Maintenance() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public MaintenanceStatus getMaintenanceStatus() {
        return maintenanceStatus;
    }

    public void setMaintenanceStatus(MaintenanceStatus maintenanceStatus) {
        this.maintenanceStatus = maintenanceStatus;
    }

    public List<MaintenanceServiceItem> getMaintenanceServices() {
        return maintenanceServices;
    }

    public void setMaintenanceServices(List<MaintenanceServiceItem> maintenanceList) {
        this.maintenanceServices = maintenanceList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Maintenance that = (Maintenance) o;
        return getVehicle().equals(that.getVehicle()) &&
                getFromDate().equals(that.getFromDate()) &&
                getToDate().equals(that.getToDate()) &&
                getMaintenanceStatus().equals(that.getMaintenanceStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVehicle(),
                getFromDate(),
                getToDate(),
                getMaintenanceStatus());
    }
}

package com.telerikacademy.smartgarage.models;

public class VehicleCreateDto extends VehicleDto{
    private String modelName;
    private String manufacturerName;
    private Integer manufacturerId;

    public VehicleCreateDto() {
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manifacturerName) {
        this.manufacturerName = manifacturerName;
    }

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }
}

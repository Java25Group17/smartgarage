package com.telerikacademy.smartgarage.models;

import java.time.LocalDate;

public class UserSearchParameters {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String model;
    private String mark;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String orderBy;
    private String enabled;

    public UserSearchParameters() {
    }

    public UserSearchParameters(String firstName,
                                String lastName,
                                String email,
                                String phone,
                                String model,
                                String mark,
                                LocalDate fromDate,
                                LocalDate toDate,
                                String orderBy,
                                String enabled) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.model = model;
        this.mark = mark;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.orderBy = orderBy;
        this.enabled = enabled;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String isEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }
}

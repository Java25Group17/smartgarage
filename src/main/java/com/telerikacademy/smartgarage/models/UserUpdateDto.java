package com.telerikacademy.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserUpdateDto {
    @NotNull(message = "Email can't be empty.")
    @Size(min = 5, max = 50, message = "Email should be between 5 and 50 symbols.")
    private String email;

    @NotNull(message = "First name can't be empty.")
    @Size(min = 2, max = 20, message = "First name should be between 2 and 20 symbols.")
    private String firstName;

    @NotNull(message = "Last name can't be empty.")
    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20 symbols.")
    private String lastName;

    @NotNull(message = "Phone can't be empty.")
    @Pattern(regexp="(^0[0-9]{9}$)", message = "Phone should be 10 symbols in format 0xxxxxxxxx.")
    private String phone;

    public UserUpdateDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}


package com.telerikacademy.smartgarage.models;

import javax.validation.constraints.NotNull;

public class MaintenanceServiceItemDto {
    @NotNull
    private Integer maintenanceId;

    @NotNull
    private Integer serviceId;

    @NotNull
    private Integer hours;

    public MaintenanceServiceItemDto() {
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getMaintenanceId() {
        return maintenanceId;
    }

    public void setMaintenanceId(Integer maintenanceId) {
        this.maintenanceId = maintenanceId;
    }
}

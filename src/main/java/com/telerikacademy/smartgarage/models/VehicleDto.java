package com.telerikacademy.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class VehicleDto {
    @NotNull(message = "Customer can't be empty")
    private Integer customerId;

    @NotNull
    @Pattern(regexp = "[A-Z]{2}[\\d]{4}[A-Z]{2}",
            message = "Registration Plate should be 2 letters, 4 digits, 2 letters")
    private String registrationPlate;

    @NotNull
    @Pattern(regexp = "[A-Za-z0-9]{17}",
            message = "Identification Number should be of 17 digits long")
    private String identificationNumber;

    @NotNull
    private Integer modelId;

    @NotNull
    private Integer vehicleCategoryId;

    @NotNull
    private String colour;

    @NotNull
    private Integer engineTypeId;

    @NotNull
    private boolean enabled;

    public VehicleDto() {
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getRegistrationPlate() {
        return registrationPlate;
    }

    public void setRegistrationPlate(String registrationPlate) {
        this.registrationPlate = registrationPlate;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getVehicleCategoryId() {
        return vehicleCategoryId;
    }

    public void setVehicleCategoryId(Integer vehicleCategoryId) {
        this.vehicleCategoryId = vehicleCategoryId;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Integer getEngineTypeId() {
        return engineTypeId;
    }

    public void setEngineTypeId(Integer engineTypeId) {
        this.engineTypeId = engineTypeId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

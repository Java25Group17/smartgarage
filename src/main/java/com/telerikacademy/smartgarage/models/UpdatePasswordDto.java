package com.telerikacademy.smartgarage.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdatePasswordDto{
    @NotNull(message = "Email can't be empty.")
    @Size(min = 5, max = 50, message = "Email should be between 5 and 50 symbols.")
    private String email;

    @NotNull(message = "Password can't be empty.")
    @Size(min = 8, max = 64, message = "Password should be between 8 and 64 symbols.")
    private String password;

    @NotNull(message = "Password can't be empty.")
    @Size(min = 8, max = 64, message = "Password should be between 8 and 64 symbols.")
    private String confirmPassword;

    public UpdatePasswordDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}

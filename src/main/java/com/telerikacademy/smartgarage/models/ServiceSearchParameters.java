package com.telerikacademy.smartgarage.models;

public class ServiceSearchParameters {
    private String name;
    private Double minPrice;
    private Double maxPrice;
    private boolean enabled;

    public ServiceSearchParameters(String name, Double minPrice, Double maxPrice, boolean enabled) {
        this.name = name;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.enabled = enabled;
    }

    public ServiceSearchParameters() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

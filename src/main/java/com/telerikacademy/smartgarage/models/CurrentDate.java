package com.telerikacademy.smartgarage.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "currency_current_date")
public class CurrentDate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "date")
    private LocalDate date;

    public CurrentDate() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}

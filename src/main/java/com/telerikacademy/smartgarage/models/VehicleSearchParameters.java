package com.telerikacademy.smartgarage.models;

public class VehicleSearchParameters {
    private Integer customerId;
    private Integer modelId;
    private Integer manufacturerId;
    private String registrationNumber;
    private boolean enabled;

    public VehicleSearchParameters() {
    }

    public VehicleSearchParameters(Integer customerId,
                                   Integer modelId,
                                   Integer manufacturerId,
                                   String registrationNumber,
                                   boolean enabled) {
        this.customerId = customerId;
        this.modelId = modelId;
        this.manufacturerId = manufacturerId;
        this.registrationNumber = registrationNumber;
        this.enabled = enabled;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

package com.telerikacademy.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "maintenance_status")
public class MaintenanceStatus {

    public static final int NOT_STARTED = 1;
    public static final int IN_PROGRESS = 2;
    public static final int READY_FOR_PICKUP = 3;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    public MaintenanceStatus() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

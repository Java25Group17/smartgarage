package com.telerikacademy.smartgarage.models;

public class ModelSearchParameters {
    private String name;
    private Integer manufacturerId;

    public ModelSearchParameters(String name, Integer manufacturerId) {
        this.name = name;
        this.manufacturerId = manufacturerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }
}

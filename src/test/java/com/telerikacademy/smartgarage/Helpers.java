package com.telerikacademy.smartgarage;

import com.telerikacademy.smartgarage.models.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class Helpers {
    public static User createMockCustomer() {
        var mockUser = new User();
        mockUser.setId(3);
        mockUser.setEmail("mock@user.com");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setPhone("MockPhone");
        mockUser.setRole(createMockCustomerRole());
        mockUser.setEnabled(true);
        return mockUser;
    }

    public static User createMockEmployee() {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setEmail("mock@employee.com");
        mockUser.setLastName("MockEmployeeName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockEmployeeFirstName");
        mockUser.setRole(createMockEmployeeRole());
        return mockUser;
    }

    public static User createMockAdmin() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@admin.com");
        mockUser.setLastName("MockAdminName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockEmployeeFirstName");
        mockUser.setRole(createMockAdminRole());
        return mockUser;
    }

    private static Role createMockCustomerRole() {
        var mockRole =  new Role();
        mockRole.setId(3);
        mockRole.setName("customer");
        return mockRole;
    }

    private static Role createMockEmployeeRole() {
        var mockRole =  new Role();
        mockRole.setId(2);
        mockRole.setName("employee");
        return mockRole;
    }

    private static Role createMockAdminRole() {
        var mockRole =  new Role();
        mockRole.setId(1);
        mockRole.setName("admin");
        return mockRole;
    }

    private static EngineType createMockEngineType() {
        var mockEngineType =  new EngineType();
        mockEngineType.setId(1);
        mockEngineType.setName("MockEngineType");
        return mockEngineType;
    }

    public static MaintenanceStatus createMockMaintenanceStatus() {
        var mockMaintenanceStatus = new MaintenanceStatus();
        mockMaintenanceStatus.setId(1);
        mockMaintenanceStatus.setName("not started");
        return mockMaintenanceStatus;
    }

    public static Manufacturer createMockManufacturer() {
        var mockManufacturer = new Manufacturer();
        mockManufacturer.setId(1);
        mockManufacturer.setName("MockManufacturer");
        return mockManufacturer;
    }

    public static Model createMockModel() {
        var mockModel = new Model();
        mockModel.setId(1);
        mockModel.setName("MockModel");
        mockModel.setManufacturer(createMockManufacturer());
        return mockModel;
    }

    public static Service createMockService() {
        var mockService = new Service();
        mockService.setId(1);
        mockService.setName("MockService");
        mockService.setPricePerHour(5);
        mockService.setEnabled(true);
        return mockService;
    }

    public static VehicleCategory createMockVehicleCategory() {
        var mockVehicleCategory = new VehicleCategory();
        mockVehicleCategory.setId(1);
        mockVehicleCategory.setName("MockVehicleCategory");
        return mockVehicleCategory;
    }

    public static Vehicle createMockVehicle() {
        var mockVehicle = new Vehicle();
        mockVehicle.setId(1);
        mockVehicle.setCustomer(createMockCustomer());
        mockVehicle.setRegistrationPlate("MockRegistrationPlate");
        mockVehicle.setIdentificationNumber("MockIdentificationNumber");
        mockVehicle.setModel(createMockModel());
        mockVehicle.setCategory(createMockVehicleCategory());
        mockVehicle.setColour("MockColour");
        mockVehicle.setEngineType(createMockEngineType());
        mockVehicle.setEnabled(true);
        return mockVehicle;
    }

    public static Maintenance createMockMaintenance() {
        var mockMaintenance = new Maintenance();
        mockMaintenance.setId(1);
        mockMaintenance.setVehicle(createMockVehicle());
        mockMaintenance.setFromDate(LocalDate.of(2021, Month.APRIL, 1));
        mockMaintenance.setToDate(LocalDate.of(2021, Month.APRIL, 16));
        mockMaintenance.setMaintenanceStatus(createMockMaintenanceStatus());

        List<MaintenanceServiceItem> maintenanceServiceItemList = new ArrayList<>();
        mockMaintenance.setMaintenanceServices(maintenanceServiceItemList);

        return mockMaintenance;
    }

    public static MaintenanceServiceItem createMockMaintenanceServiceItem() {
        var mockMaintenanceServiceItem = new MaintenanceServiceItem();
        mockMaintenanceServiceItem.setId(1);
        mockMaintenanceServiceItem.setMaintenance(createMockMaintenance());
        mockMaintenanceServiceItem.setService(createMockService());
        mockMaintenanceServiceItem.setHours(1);
        return mockMaintenanceServiceItem;
    }

    public static MaintenanceSearchParameters createMockMaintenanceSearchParameters() {
        var mockSearchParameters = new MaintenanceSearchParameters();
        return mockSearchParameters;
    }
}

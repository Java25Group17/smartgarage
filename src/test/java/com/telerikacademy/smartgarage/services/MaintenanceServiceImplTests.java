package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.MaintenanceSearchParameters;
import com.telerikacademy.smartgarage.models.MaintenanceServiceItem;
import com.telerikacademy.smartgarage.models.VehicleSearchParameters;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import com.telerikacademy.smartgarage.repositories.contracts.CurrencyRepository;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceServiceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class MaintenanceServiceImplTests {
    @Mock
    MaintenanceRepository maintenanceRepository;

    @Mock
    MaintenanceServiceRepository maintenanceServiceRepository;

    @Mock
    CurrencyRepository currencyRepository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    MaintenanceServiceImpl maintenanceService;

    @Test
    public void getById_Should_CallRepository() {
        //Arrange
        var mockUser = Helpers.createMockEmployee();

        // Act
        maintenanceService.getById(1, mockUser);

        //Assert
        Mockito.verify(maintenanceRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void getAll_Should_ThrowException_WhenEmployeeAndIsDisabled() {
        //Arrange
        //  var mockUser = Helpers.createMockAdmin();
        var mockUser = Helpers.createMockEmployee();

        //Act,Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> authorisationHelper.verifyUserHasRoles(mockUser, "mockMessage", UserRoles.ADMIN));
    }

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        //  var mockUser = Helpers.createMockAdmin();
        var mockUser = Helpers.createMockEmployee();
        MaintenanceSearchParameters searchParameters = new MaintenanceSearchParameters
                (0, 0, 0, null, null, "test");

        //Assert
        assertThrows(IllegalEntityException.class,
                () -> maintenanceService.getAll(mockUser, searchParameters));
    }

    @Test
    public void getAll_Should_CallRepository_WhenOrderByDateIsNull() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        // var mockUser = Helpers.createMockEmployee();
        MaintenanceSearchParameters searchParameters = new MaintenanceSearchParameters
                (0, 0, 0, null, null, null);

        //Act
        maintenanceService.getAll(mockUser, searchParameters);

        //Assert
        Mockito.verify(maintenanceRepository, Mockito.times(1))
                .getAll(searchParameters);
    }

    @Test
    public void create_Should_ThrowException_When_VehicleExists() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository
                .getByNotFinishedVehicle(mockMaintenance.getVehicle()))
                .thenReturn(mockMaintenance);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> maintenanceService.create(mockUser, mockMaintenance));
    }

    @Test
    public void create_Should_CallVehicleRepository() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository
                .getByNotFinishedVehicle(mockMaintenance.getVehicle()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        maintenanceService.create(mockUser, mockMaintenance);

        //Assert
        Mockito.verify(maintenanceRepository, Mockito.times(1))
                .create(mockMaintenance);
    }

    @Test
    public void update_Should_ThrowException_When_MaintenanceNotExists() {
        //Arrange//
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository
                .getByNotFinishedVehicle(mockMaintenance.getVehicle()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> maintenanceService.update(mockMaintenance, mockUser));
    }

    @Test
    public void update_Should_CallVehicleRepository() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository
                .getByNotFinishedVehicle(mockMaintenance.getVehicle()))
                .thenReturn(mockMaintenance);

        //Act
        maintenanceService.update(mockMaintenance, mockUser);

        //Assert
        Mockito.verify(maintenanceRepository, Mockito.times(1))
                .update(mockMaintenance);
    }

    @Test
    public void addService_Should_ThrowException_When_MaintenanceIsNotExists() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();
        Set<MaintenanceServiceItem> serviceItems = new HashSet<>();
        authorisationHelper.verifyNotCustomer(mockUser);
        Mockito.when(maintenanceRepository.getById(mockMaintenance.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> maintenanceService.addServices(mockMaintenance.getId(),
                        serviceItems, mockUser));
    }

    @Test
    public void addServices_Should_ThrowException_When_MaintenanceToDateNotNull() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();
        Set<MaintenanceServiceItem> serviceItems = new HashSet<>();
        mockMaintenance.setToDate(LocalDate.now());
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository.getById(mockMaintenance.getId()))
                .thenReturn(mockMaintenance);

        // Act, Assert
        assertThrows(IllegalEntityException.class,
                () -> maintenanceService.addServices(mockMaintenance.getId(),
                        serviceItems, mockUser));
    }

    @Test
    public void addServices_Should_ThrowException_When_MaintenanceToDateIsNull() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();
        var mockMaintenanceServiceItem = Helpers.createMockMaintenanceServiceItem();
        Set<MaintenanceServiceItem> serviceItems = new HashSet<>();
        mockMaintenance.setToDate(null);

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository.getById(mockMaintenance.getId()))
                .thenReturn(mockMaintenance);

        serviceItems.add(mockMaintenanceServiceItem);
        List<MaintenanceServiceItem> maintenanceServices = mockMaintenance.getMaintenanceServices();
        maintenanceServices.addAll(serviceItems);
        mockMaintenance.setMaintenanceServices(maintenanceServices);
        //Act
        maintenanceService.addServices(mockMaintenance.getId(), serviceItems, mockUser);

        //Assert
        Mockito.verify(maintenanceRepository, Mockito.times(1))
                .update(mockMaintenance);
    }

    @Test
    public void removeService_Should_ThrowException_When_UserIsCustomer() {
        // Arrange
        var mockUser = Helpers.createMockCustomer();
        var mockMaintenance = Helpers.createMockMaintenance();
   //     authorisationHelper.verifyNotCustomer(mockUser);

        // Act, Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> maintenanceService.removeService(mockMaintenance.getId(), 1, mockUser));
    }

    @Test
    public void removeService_Should_ThrowException_When_MaintenanceIsNotExists() {
        // Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository.getById(mockMaintenance.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> maintenanceService.removeService(mockMaintenance.getId(), 1, mockUser));
    }

    @Test
    public void removeService_Should_ThrowException_When_MaintenanceToDateNotNull() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();
        var mockService = Helpers.createMockService();
        mockMaintenance.setToDate(LocalDate.now());
        var mockServiceMaintenance = Helpers.createMockMaintenanceServiceItem();
        ArrayList<MaintenanceServiceItem> items = new ArrayList<MaintenanceServiceItem>();
        items.add(mockServiceMaintenance);
        mockMaintenance.setMaintenanceServices(items);
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository.getById(mockMaintenance.getId()))
                .thenReturn(mockMaintenance);

        // Act, Assert
        assertThrows(IllegalEntityException.class,
                () -> maintenanceService.removeService(mockMaintenance.getId(),
                        mockServiceMaintenance.getId(),  mockUser));
    }
    @Test
    public void removeService_Should_ThrowException_When_ServiceItemNotInMaintenance() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();
        mockMaintenance.setToDate(null);
        var mockServiceMaintenance = Helpers.createMockMaintenanceServiceItem();
        ArrayList<MaintenanceServiceItem> items = new ArrayList<MaintenanceServiceItem>();
        items.add(mockServiceMaintenance);
        mockMaintenance.setMaintenanceServices(items);
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository.getById(mockMaintenance.getId()))
                .thenReturn(mockMaintenance);

        // Act, Assert
        assertThrows(IllegalEntityException.class,
                () -> maintenanceService.removeService(mockMaintenance.getId(),
                        2,  mockUser));
    }

    @Test
    public void removeService_Should_CallRepository() {
        // Arrange
        var mockAuthenticator = Helpers.createMockEmployee();
        var mockUser = Helpers.createMockAdmin();
        var mockMaintenance = Helpers.createMockMaintenance();
        mockMaintenance.setToDate(null);
        var mockServiceMaintenance = Helpers.createMockMaintenanceServiceItem();
        ArrayList<MaintenanceServiceItem> items = new ArrayList<MaintenanceServiceItem>();
        items.add(mockServiceMaintenance);
        mockMaintenance.setMaintenanceServices(items);
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository.getById(mockMaintenance.getId()))
                .thenReturn(mockMaintenance);
        Mockito.when(maintenanceServiceRepository.getById(mockServiceMaintenance.getId()))
                .thenReturn(mockServiceMaintenance);
        //Act
        maintenanceService.removeService(mockMaintenance.getId(),mockServiceMaintenance.getId(),mockUser);

        //Assert
        Mockito.verify(maintenanceRepository, Mockito.times(1)).update(mockMaintenance);
    }


    @Test
    public void sendPDFByEmail_Should_ThrowException_When_NoVehicleAndNoCustomerSelected() {
        //Arrange
        var mockMaintenanceSearchParameters = Helpers.createMockMaintenanceSearchParameters();
        var currencyAbbreviation = "BGN";

        // Act, Assert
        assertThrows(IllegalEntityException.class,
                () -> maintenanceService.sendPDFByMail(mockMaintenanceSearchParameters, currencyAbbreviation));
    }


}

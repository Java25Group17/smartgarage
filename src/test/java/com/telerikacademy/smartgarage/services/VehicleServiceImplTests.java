package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.VehicleSearchParameters;
import com.telerikacademy.smartgarage.models.enums.UserRoles;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceImplTests {
    @Mock
    VehicleRepository vehicleRepository;

    @Mock
    MaintenanceRepository maintenanceRepository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    VehicleServiceImpl vehicleService;

    @Test
    public void getById_Should_CallRepository() {
        //Arrange, Act
        vehicleService.getById(1);

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void getAll_Should_ThrowException_WhenEmployeeAndIsDisabled() {
        //Arrange
        //  var mockUser = Helpers.createMockAdmin();
        var mockUser = Helpers.createMockEmployee();

        //Act,Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> authorisationHelper.verifyUserHasRoles(mockUser, "mockMessage", UserRoles.ADMIN));
    }

    @Test
    public void getAll_Should_CallRepository_WhenIsEnabled() {
        //Arrange
        //  var mockUser = Helpers.createMockAdmin();
        var mockUser = Helpers.createMockEmployee();
        VehicleSearchParameters searchParameters = new VehicleSearchParameters
                (0, 0, 0, null, true);

        //Act
        vehicleService.getAll(mockUser, searchParameters);

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1)).getAll(searchParameters);
    }

    @Test
    public void getAll_Should_CallRepository_WhenIsDisabled() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        // var mockUser = Helpers.createMockEmployee();
        VehicleSearchParameters searchParameters = new VehicleSearchParameters
                (0, 0, 0, null, false);

        //Act
        vehicleService.getAll(mockUser, searchParameters);

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1)).getAll(searchParameters);
    }

    @Test
    public void create_Should_ThrowException_When_VehicleExists() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockVehicle = Helpers.createMockVehicle();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleRepository
                .getByIdentificationNumber(mockVehicle.getIdentificationNumber()))
                .thenReturn(mockVehicle);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> vehicleService.create(mockVehicle, mockUser));
    }

    @Test
    public void create_Should_CallVehicleRepository() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockVehicle = Helpers.createMockVehicle();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleRepository
                .getByIdentificationNumber(mockVehicle.getIdentificationNumber()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        vehicleService.create(mockVehicle, mockUser);

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1))
                .create(mockVehicle);
    }

    @Test
    public void update_Should_ThrowException_When_VehicleNotExists() {
        //Arrange//
        var mockUser = Helpers.createMockAdmin();
        var mockVehicle = Helpers.createMockVehicle();
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleRepository
                .getById(mockVehicle.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> vehicleService.update(mockVehicle, mockUser));
    }

    @Test
    public void update_Should_CallVehicleRepository() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockVehicle = Helpers.createMockVehicle();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleRepository
                .getById(mockVehicle.getId()))
                .thenReturn(mockVehicle);

        //Act
        vehicleService.update(mockVehicle, mockUser);

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1))
                .update(mockVehicle);
    }

    @Test
    public void delete_Should_ThrowException_When_VehicleNotReadyForPickup() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockVehicle = Helpers.createMockVehicle();
        var mockMaintenance = Helpers.createMockMaintenance();
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(maintenanceRepository
                .selectCountVehicleInMaintenance(mockVehicle.getId()))
                .thenReturn((long)1);
        // Act, Assert
        assertThrows(IllegalEntityException.class,
                () -> vehicleService.delete(mockVehicle.getId(), mockUser));
    }
//--------------
    @Test
    public void delete_Should_ThrowException_When_VehicleNotMaintenance() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockVehicle = Helpers.createMockVehicle();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleRepository
                .getById(mockVehicle.getId()))
                .thenReturn(mockVehicle);

        //Act
        vehicleService.update(mockVehicle, mockUser);

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1))
                .update(mockVehicle);
    }

//    @Test
//    public void delete_Should_ThrowException_When_VehicleNotExists() {
//        //Arrange
//        var mockUser = Helpers.createMockAdmin();
//        var mockVehicle = Helpers.createMockVehicle();
//
//        authorisationHelper.verifyNotCustomer(mockUser);
//
////        vehicleService.delete(2, mockUser);
//
//
//        // Act, Assert
//        assertThrows(EntityNotFoundException.class,
//                () -> vehicleService.delete(2, mockUser));
//    }
}

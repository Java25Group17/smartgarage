package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleCategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class VehicleCategoryServiceImplTests {
    @Mock
    VehicleCategoryRepository vehicleCategoryRepository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    VehicleCategoryServiceImpl vehicleCategoryService;

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange, Act
        vehicleCategoryService.getAll();

        //Assert
        Mockito.verify(vehicleCategoryRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange, Act
        vehicleCategoryService.getById(1);

        //Assert
        Mockito.verify(vehicleCategoryRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void create_Should_ThrowException_When_VehicleCategoryExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockVehicleCategory = Helpers.createMockVehicleCategory();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleCategoryRepository.getByName(mockVehicleCategory.getName()))
                .thenReturn(mockVehicleCategory);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> vehicleCategoryService.create(mockVehicleCategory, mockUser));
    }

    @Test
    public void create_Should_CallVehicleCategoryRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockVehicleCategory = Helpers.createMockVehicleCategory();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleCategoryRepository.getByName(mockVehicleCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        vehicleCategoryService.create(mockVehicleCategory, mockUser);

        //Assert
        Mockito.verify(vehicleCategoryRepository, Mockito.times(1))
                .create(mockVehicleCategory);
    }

    @Test
    public void update_Should_ThrowException_When_VehicleCategoryNotExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockVehicleCategory = Helpers.createMockVehicleCategory();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleCategoryRepository.getByName(mockVehicleCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> vehicleCategoryService.update(mockVehicleCategory, mockUser));
    }

    @Test
    public void update_Should_CallVehicleCategoryRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockVehicleCategory = Helpers.createMockVehicleCategory();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(vehicleCategoryRepository.getByName(mockVehicleCategory.getName()))
                .thenReturn(mockVehicleCategory);

        //Act
        vehicleCategoryService.update(mockVehicleCategory, mockUser);

        //Assert
        Mockito.verify(vehicleCategoryRepository, Mockito.times(1))
                .update(mockVehicleCategory);
    }
}

package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.models.UserSearchParameters;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UserRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehicleRepository;
import com.telerikacademy.smartgarage.utils.MailSender;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository repository;
    @Mock
    VehicleRepository vehicleRepository;
    @Mock
    MaintenanceRepository maintenanceRepository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    UserServiceImpl service;

    @Mock
    MailSender mailSender;


    @Test
    public void getAll_Should_CallRepository() {
        //Arrange, Act
        var mockUser = Helpers.createMockEmployee();
        service.getAll(mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void filterByMultiple_Should_CallRepository() {
        //Arrange, Act
        var mockUser = Helpers.createMockEmployee();
        var mockSearchParameters = new UserSearchParameters();

        service.filterByMultiple(mockSearchParameters, mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).filterByMultiple(mockSearchParameters);
    }

    @Test
    public void filterByMultiple_Should_ThrowError_When_SearchOrderWrong() {
        //Arrange
        var mockUser = Helpers.createMockEmployee();
        var mockSearchParameters = new UserSearchParameters();
        mockSearchParameters.setOrderBy("Test");

        //Act, Assert
        assertThrows(IllegalEntityException.class,
                () -> service.filterByMultiple(mockSearchParameters, mockUser));
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange, Act
        var mockUser = Helpers.createMockAdmin();

        service.getById(1, mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getById(1);
    }

    @Test
    public void getEmail_Should_CallRepository() {
        //Arrange, Act
        var email = "test@test.email";
        service.getByEmail(email);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getByEmail(email);
    }

    @Test
    public void create_Should_ThrowException_When_CustomerWithSameEmailExists() {
        //Arrange

        var mockUser = Helpers.createMockEmployee();
        var mockCustomer = Helpers.createMockCustomer();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getByEmail(mockCustomer.getEmail()))
                .thenReturn(mockCustomer);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockCustomer, mockUser));
    }

//    @Test
//    public void create_Should_ThrowException_When_EmailDontExist() {
//        //Arrange
//
//        var mockUser = Helpers.createMockEmployee();
//        var mockCustomer = Helpers.createMockCustomer();
//
//        authorisationHelper.verifyNotCustomer(mockUser);
//
//        Mockito.when(repository.getByEmail(mockCustomer.getEmail()))
//                .thenThrow(new EntityNotFoundException(""));
//
//        Mockito.when(mailSender.sendMail("","",""))
//                .thenReturn(false);
//        // Act, Assert
//        assertThrows(AuthenticationFailureException.class,
//                () -> service.create(mockCustomer, mockUser));
//
//    }

    @Test
    public void create_Should_CallRepositoryCreate_When_CustomerNotExists() {
        //Arrange

        var mockUser = Helpers.createMockEmployee();
        var mockCustomer = Helpers.createMockCustomer();

        authorisationHelper.verifyNotCustomer(mockUser);
        Mockito.when(repository.getByEmail(mockCustomer.getEmail()))
                .thenThrow(new EntityNotFoundException(""));
        //Act
        service.create(mockCustomer, mockUser);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).create(mockCustomer);
    }

    @Test
    public void update_Should_ThrowException_When_CustomerWithSameMailExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockCustomer = Helpers.createMockCustomer();
        var mockToUpdate = Helpers.createMockCustomer();
        mockToUpdate.setId(mockCustomer.getId() + 1);

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getByEmail(mockToUpdate.getEmail()))
                .thenReturn(mockCustomer);


        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> service.update(mockToUpdate, mockUser));
    }

    @Test
    public void update_Should_CallRepository_When_NoCustomerWithMailExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockCustomer = Helpers.createMockCustomer();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getByEmail(mockCustomer.getEmail()))
                .thenThrow(new EntityNotFoundException(""));


        //Act
        service.update(mockCustomer, mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(mockCustomer);
    }

    @Test
    public void update_Should_CallRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockCustomer = Helpers.createMockCustomer();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getByEmail(mockCustomer.getEmail()))
                .thenReturn(mockCustomer);

        //Act
        service.update(mockCustomer, mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(mockCustomer);
    }

    @Test
    public void delete_Should_DoNothing_When_ActiveMaintenanceExists() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockCustomer = Helpers.createMockCustomer();
        authorisationHelper.verifyNotCustomer(mockUser);
        Mockito.when(maintenanceRepository.selectCountVehicleForClient(mockCustomer.getId()))
                .thenReturn(Long.parseLong("1"));
        // Act, Assert
        service.delete(mockCustomer.getId(), mockUser);
        Mockito.verify(repository, Mockito.times(0)).update(mockCustomer);
    }

    @Test
    public void delete_Should_CallRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockCustomer = Helpers.createMockCustomer();
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getById(mockCustomer.getId()))
                .thenReturn(mockCustomer);

        Mockito.when(maintenanceRepository.selectCountVehicleForClient(mockCustomer.getId()))
                .thenReturn(Long.parseLong("0"));

        //Act
        service.delete(mockCustomer.getId(), mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(mockCustomer);
    }

    @Test
    public void delete_Should_CallVehicleRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockCustomer = Helpers.createMockCustomer();
        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getById(mockCustomer.getId()))
                .thenReturn(mockCustomer);

        Mockito.when(maintenanceRepository.selectCountVehicleForClient(mockCustomer.getId()))
                .thenReturn(Long.parseLong("0"));

        //Act
        service.delete(mockCustomer.getId(), mockUser);

        //Assert
        Mockito.verify(vehicleRepository, Mockito.times(1)).disableVehicles(mockCustomer.getId());
    }
}

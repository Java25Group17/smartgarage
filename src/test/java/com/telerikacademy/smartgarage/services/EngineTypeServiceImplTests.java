package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.repositories.contracts.EngineTypeRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class EngineTypeServiceImplTests {

    @Mock
    EngineTypeRepository repository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    EngineTypeServiceImpl service;


    @Test
    public void getAll_Should_CallRepository() {
        //Arrange, Act
        service.getAll();

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange, Act
        service.getById(1);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getById(1);
    }
}

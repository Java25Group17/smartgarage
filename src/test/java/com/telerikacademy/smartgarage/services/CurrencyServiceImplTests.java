package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Currency;
import com.telerikacademy.smartgarage.models.CurrentDate;
import com.telerikacademy.smartgarage.repositories.contracts.CurrencyRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.smartgarage.utils.ExchangeRates_DOM;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceImplTests {
    @Mock
    CurrencyRepository repository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    CurrencyServiceImpl service;

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange, Act
        service.getAll(Helpers.createMockEmployee());

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange, Act
        service.getById(1,Helpers.createMockEmployee());

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getById(1);
    }

   @Test
    public void update_Should_DoNothing_When_CurrentDateIsNow() {
        //Arrange
        var mockCurrentDate = new CurrentDate();
        var mockCurrency = new Currency();
        mockCurrentDate.setDate(LocalDate.now());

        Mockito.when(repository.getDate())
                .thenReturn(mockCurrentDate);

        // Act
       service.update();
       // , Assert
       Mockito.verify(repository,Mockito.times(0)).update(mockCurrency);
    }

//    @Test
//    public void update_Should_DoNothing_When_ExchangeRatesIsEmpty() {
//        //Arrange
//        var mockCurrentDate = new CurrentDate();
//        var mockCurrency = new Currency();
//        var now = LocalDate.now();
//        var date = LocalDate.of(now.getYear(),now.getMonth(),now.getDayOfMonth()-1);
//        mockCurrentDate.setDate(date);
//        var currencies = new ArrayList<Currency>();
//
//        Mockito.when(repository.getDate())
//                .thenReturn(mockCurrentDate);
//
//        var exchangeRates_dom = new ExchangeRates_DOM(currencies);
//
//        Mockito.when(exchangeRates_dom.getExchangeRates())
//                .thenReturn(new HashMap<String,Double>());
//        // Act
//        service.update();
//        // , Assert
//        Mockito.verify(repository,Mockito.times(0)).update(mockCurrency);
//    }

//    @Test
//    public void update_Should_CallManufacturerRepository() {
//        //Arrange
//
//        var mockUser = Helpers.createMockAdmin();
//        var mockManufacturer = Helpers.createMockManufacturer();
//
//        authorisationHelper.verifyNotCustomer(mockUser);
//
//        Mockito.when(manufacturerRepository.getByName(mockManufacturer.getName()))
//                .thenReturn(mockManufacturer);
//
//        //Act
//        manufacturerService.update(mockManufacturer, mockUser);
//
//        //Assert
//        Mockito.verify(manufacturerRepository, Mockito.times(1)).update(mockManufacturer);
//    }*/

}

package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ModelServiceImplTests {
    @Mock
    ModelRepository modelRepository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    ModelServiceImpl modelService;


    @Test
    public void getAll_Should_CallRepository() {
        //Arrange, Act
        modelService.getAll(null);

        //Assert
        Mockito.verify(modelRepository, Mockito.times(1)).getAll(null);
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange, Act
        modelService.getById(1);

        //Assert
        Mockito.verify(modelRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void create_Should_ThrowException_When_ModelExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockModel = Helpers.createMockModel();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(modelRepository.getByName(mockModel.getName()))
                .thenReturn(mockModel);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> modelService.create(mockModel, mockUser));
    }

    @Test
    public void create_Should_CallModelRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockModel = Helpers.createMockModel();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(modelRepository.getByName(mockModel.getName()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        modelService.create(mockModel, mockUser);

        //Assert
        Mockito.verify(modelRepository, Mockito.times(1)).create(mockModel);
    }

    @Test
    public void update_Should_ThrowException_When_ModelNotExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockModel = Helpers.createMockModel();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(modelRepository.getByName(mockModel.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> modelService.update(mockModel, mockUser));
    }

    @Test
    public void update_Should_CallModelRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockModel = Helpers.createMockModel();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(modelRepository.getByName(mockModel.getName()))
                .thenReturn(mockModel);

        //Act
        modelService.update(mockModel, mockUser);

        //Assert
        Mockito.verify(modelRepository, Mockito.times(1)).update(mockModel);
    }
}

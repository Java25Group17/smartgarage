package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.repositories.contracts.EngineTypeRepository;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceStatusRepository;
import com.telerikacademy.smartgarage.services.contracts.StatusService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {

    @Mock
    MaintenanceStatusRepository repository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    StatusServiceImpl service;

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange, Act
        service.getAll();

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }
}

package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.models.ServiceSearchParameters;
import com.telerikacademy.smartgarage.repositories.contracts.MaintenanceServiceRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ServiceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ServiceServiceImplTests {

    @Mock
    ServiceRepository repository;

    @Mock
    MaintenanceServiceRepository maintenanceServiceRepository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    ServiceServiceImpl service;


    @Test
    public void getAll_Should_CallRepository() {
        //Arrange, Act
        var mockParameters = new ServiceSearchParameters();
        service.getAll(mockParameters);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll(mockParameters);
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange, Act
        service.getById(1);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getById(1);
    }

    @Test
    public void create_Should_ThrowException_When_ServiceExistsAndEnabled() {
        //Arrange

        var mockUser = Helpers.createMockEmployee();
        var mockService = Helpers.createMockService();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getByName(mockService.getName()))
                .thenReturn(mockService);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockService, mockUser));
    }


    @Test
    public void create_Should_CallRepositoryCreate_When_ServiceNotExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockService = Helpers.createMockService();
        authorisationHelper.verifyNotCustomer(mockUser);
        Mockito.when(repository.getByName(mockService.getName()))
                .thenThrow(EntityNotFoundException.class);
        //Act
        service.create(mockService, mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).create(mockService);
    }
    @Test
    public void create_Should_CallRepositoryUpdate_When_ServiceDisabled() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockService = Helpers.createMockService();
        mockService.setEnabled(false);

        var mockServiceToCreate = Helpers.createMockService();
        authorisationHelper.verifyNotCustomer(mockUser);

       Mockito.when(repository.getByName(mockService.getName()))
                .thenReturn(mockService);
        //Act
        service.create(mockServiceToCreate, mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(mockService);
    }

    @Test
    public void update_Should_ThrowException_When_ServiceNotExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockService = Helpers.createMockService();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getByName(mockService.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> service.update(mockService, mockUser));
    }

    @Test
    public void update_Should_ThrowException_When_ServiceWithSameNameExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockService = Helpers.createMockService();
        var mocktoUpdate = Helpers.createMockService();
        mocktoUpdate.setId(mockService.getId()+1);

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getByName(mocktoUpdate.getName()))
                .thenReturn(mockService);


       // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> service.update(mocktoUpdate, mockUser));
    }

    @Test
    public void update_Should_CallRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockService = Helpers.createMockService();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(repository.getByName(mockService.getName()))
                .thenReturn(mockService);

        //Act
        service.update(mockService, mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(mockService);
    }
    @Test
    public void delete_Should_ThrowException_When_ActiveMaintenanceWithSameNameExists() {
        //Arrange
        var mockUser = Helpers.createMockAdmin();
        var mockService = Helpers.createMockService();
        authorisationHelper.verifyNotCustomer(mockUser);
        Mockito.when(maintenanceServiceRepository.selectCountServiceInUse(1))
                .thenReturn(Long.parseLong("1"));
        // Act, Assert
        assertThrows(IllegalEntityException.class,
                () -> service.delete(mockService.getId(), mockUser));
    }

    @Test
    public void delete_Should_CallRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockService = Helpers.createMockService();
        authorisationHelper.verifyNotCustomer(mockUser);
        Mockito.when(maintenanceServiceRepository.selectCountServiceInUse(1))
                .thenReturn(Long.parseLong("0"));
        Mockito.when(repository.getById(mockService.getId()))
                .thenReturn(mockService);
        //Act
        service.delete(mockService.getId(), mockUser);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(mockService);
    }
}

package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class AuthorisationHelperTests {
    @Mock
    AuthorisationHelper authorisationHelper;

    @Test
    public void update_Should_ThrowException_When_UserIsNotAdmin() {
        // Arrange
        var mockUser = Helpers.createMockCustomer();

        // Act, Assert
        assertThrows(UnauthorizedOperationException.class,
                () -> authorisationHelper.verifyNotCustomer(mockUser));
    }
}

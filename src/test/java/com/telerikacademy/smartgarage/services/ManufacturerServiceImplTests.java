package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.Helpers;
import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.IllegalEntityException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Manufacturer;
import com.telerikacademy.smartgarage.repositories.contracts.ManufacturerRepository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ManufacturerServiceImplTests {

    @Mock
    ManufacturerRepository manufacturerRepository;

    @Mock
    AuthorisationHelper authorisationHelper;

    @InjectMocks
    ManufacturerServiceImpl manufacturerService;


    @Test
    public void getAll_Should_CallRepository() {
        //Arrange, Act
        manufacturerService.getAll(null);

        //Assert
        Mockito.verify(manufacturerRepository, Mockito.times(1)).getAll(null);
    }

    @Test
    public void getById_Should_CallRepository() {
        //Arrange, Act
        manufacturerService.getById(1);

        //Assert
        Mockito.verify(manufacturerRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void create_Should_ThrowException_When_ManufacturerExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockManufacturer = Helpers.createMockManufacturer();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(manufacturerRepository.getByName(mockManufacturer.getName()))
                .thenReturn(mockManufacturer);

        // Act, Assert
        assertThrows(DuplicateEntityException.class,
                () -> manufacturerService.create(mockManufacturer, mockUser));
    }

    @Test
    public void create_Should_CallManufacturerRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockManufacturer = Helpers.createMockManufacturer();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(manufacturerRepository.getByName(mockManufacturer.getName()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        manufacturerService.create(mockManufacturer, mockUser);

        //Assert
        Mockito.verify(manufacturerRepository, Mockito.times(1)).create(mockManufacturer);
    }

    @Test
    public void update_Should_ThrowException_When_ManufacturerNotExists() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockManufacturer = Helpers.createMockManufacturer();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(manufacturerRepository.getByName(mockManufacturer.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        assertThrows(EntityNotFoundException.class,
                () -> manufacturerService.update(mockManufacturer, mockUser));
    }

    @Test
    public void update_Should_CallManufacturerRepository() {
        //Arrange

        var mockUser = Helpers.createMockAdmin();
        var mockManufacturer = Helpers.createMockManufacturer();

        authorisationHelper.verifyNotCustomer(mockUser);

        Mockito.when(manufacturerRepository.getByName(mockManufacturer.getName()))
                .thenReturn(mockManufacturer);

        //Act
        manufacturerService.update(mockManufacturer, mockUser);

        //Assert
        Mockito.verify(manufacturerRepository, Mockito.times(1)).update(mockManufacturer);
    }
}
